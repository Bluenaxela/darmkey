pixels = []
with open('FONTDT.data', 'rb') as f:
	while (True):
		d = f.read(4)
		if (d is None) or (len(d) != 4):
			break
		if (d[0] == 0):
			pixels.append(False)
		else:
			pixels.append(True)

def colsToByte(cols):
	b = 0
	for i in range(8):
		if cols[i]:
			b |= (1 << (7-i))
	return b

with open('FONTDT', 'wb') as f:
	for chrIdx in range(192):
		chrCol = chrIdx % 16
		chrRow = chrIdx // 16
		for row in range(8):
			chrOffset = chrCol * 16 + chrRow * 8 * 256
			chrRowOffset = chrOffset + row * 256
			rowdata = pixels[chrRowOffset : chrRowOffset + 16]
			f.write(bytes([colsToByte(rowdata[0:8])]))
			f.write(bytes([colsToByte(rowdata[8:16])]))


