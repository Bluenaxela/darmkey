#ifndef _ALTERMAGIC_H_
#define _ALTERMAGIC_H_

#include <vector>
#include <string>
#include <filesystem>
class DarmFS;

namespace AlterMagic
{
	enum YsFileType
	{
		FILETYPE_NONE,
		
		FILETYPE_YS1_MESSAG,
		FILETYPE_YS1_SHPMS,
		FILETYPE_YS1_BMESAG,
		
		FILETYPE_YS2_SHPMS,
		FILETYPE_YS2_EQPNAM,
		FILETYPE_YS2_EVNMS,
		FILETYPE_YS2_YSBKMS,
		FILETYPE_YS2_MESCT,
		
		FILETYPE_YS2_TTLPRG,
		FILETYPE_YS2_ZDKPRG,
		FILETYPE_YS2_MANPR1,
		FILETYPE_YS2_ZDKPR,
		FILETYPE_YS2_PRG_B800,
		
		FILETYPE_YS3_MES
	};
	
	enum YsTextEncoding
	{
		CHARSET_UNKNOWN,
		CHARSET_YS1_FONT,
		CHARSET_YS2_SHIFTJIS,
		CHARSET_YS2_SHIFTJIS_WIDEONLY
	};
	
	enum YsDisk
	{
		DISK_UNKNOWN,
		
		DISK_YS1_A,
		DISK_YS1_B,
		DISK_YS2_OPENING,
		DISK_YS2_A,
		DISK_YS2_B,
		
		DISK_YS3_OPENING,
		DISK_YS3_SCENARIO,
		DISK_YS3_DATA1,
		DISK_YS3_DATA2
	};
	
	std::wstring decodeString(YsTextEncoding encoding, const std::vector<uint8_t>& s, bool assumeEnFont);
	void encodeString(YsTextEncoding encoding, std::vector<uint8_t>& s, const std::wstring& str);
	
	YsDisk detectYsDisk(const DarmFS& fs);
	int getYsNumber(YsDisk disk);
	YsFileType getYsFileType(int ysNumber, const std::string& filename);
	YsTextEncoding getYsTextEncoding(YsFileType ysFileType);
	
	class TextFile
	{
	private:
		YsFileType mYsFileType;
		YsTextEncoding mTextEncoding;
		YsDisk mYsDisk;
		uint32_t mMetaTableLen;
		std::vector<uint32_t> mSubTableLen;
		uint32_t mTotalTableLen;
		uint32_t mFileOffset;
		std::vector<std::wstring> mStrings;
		
		std::vector<uint32_t> mBinPtrAddresses;
		std::vector<uint8_t> mBinData;
		std::vector<uint8_t> mBinFreeSpace;
	private:
		void writeStringWithPtr(
			std::vector<uint8_t>& s,
			std::unordered_map<std::wstring,uint32_t>& existingMessages,
			uint32_t tblOffset,
			uint32_t idx,
			std::wstring str) const;
	private:
		std::vector<uint8_t> toEncodedBytesPrg() const;
	public:
		TextFile();
		~TextFile();
		bool load(const DarmFS& fs, const std::string& filename, bool assumeEnFont);
		std::vector<uint8_t> toEncodedBytes() const;
		std::wstring exportTextToWString() const;
		bool importTextFromWString(std::wstring str);
		std::string exportTextToUTF8() const;
		bool importTextFromUTF8(std::string str);
		
		static std::vector<std::string> exportAllText(const DarmFS& fs, const std::filesystem::path& path, bool assumeEnFont);
		static std::vector<std::string> importAllText(DarmFS& fs, const std::filesystem::path& path);
		
		inline YsFileType getFileType() const { return mYsFileType; };
		inline YsTextEncoding getTextEncoding() const { return mTextEncoding; };
		inline YsDisk getDiskType() const { return mYsDisk; };
		inline uint32_t getMetaTableLen() const { return mMetaTableLen; };
		inline uint32_t getFileOffset() const { return mFileOffset; };
		inline uint32_t getTotalTableLength() const { return mTotalTableLen; };
		inline uint32_t getSubTableCount() const { return mSubTableLen.size(); };
		inline const std::vector<uint32_t> & getSubTableLengths() const { return mSubTableLen; };
		inline const std::vector<std::wstring> & getStrings() const { return mStrings; };
		
		inline void setString(int flatIdx, const std::wstring& str)
		{
			if ((flatIdx >= 0) && (flatIdx < mStrings.size()))
			{
				mStrings[flatIdx] = str;
			}
		}
	};
}

#endif // _ALTERMAGIC_H_
