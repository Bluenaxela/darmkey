#include <vector>
#include <iostream>
#include <sstream>
#include <cstdio>
#include <cstring>
#include <filesystem>

#include <boost/program_options.hpp>

#include "d88.h"
#include "darmfs.h"
#include "altermagic.h"
#include "fs_compat.h"

#if !defined(_DARMKEY_CMDONLY_)
	#include <nana/gui.hpp>
	#include <nana/gui/filebox.hpp>
	#include <nana/gui/widgets/label.hpp>
	#include <nana/gui/widgets/listbox.hpp>
	#include <nana/gui/widgets/button.hpp>
	#include <nana/gui/widgets/combox.hpp>
	#include "altermagic_ui.h"
#endif // !defined(_DARMKEY_CMDONLY_)

static std::string generateExportTextMessage(const std::vector<std::string>& fileList)
{
	if (fileList.size() == 0)
	{
		return "No text files to export.";
	}

	std::stringstream s;	
	s << "Exported the following text files:\n";
	for (std::string entry : fileList)
	{
		s << "\t" << entry << "\n";
	}
	return s.str();
}

static std::string generateImportTextMessage(const std::vector<std::string>& fileList)
{
	if (fileList.size() == 0)
	{
		return "No text files were updated.";
	}

	std::stringstream s;	
	s << "Updated the following text files:\n";
	for (std::string entry : fileList)
	{
		s << "\t" << entry << "\n";
	}
	return s.str();
}

static bool saveFilesystem(const std::filesystem::path& path, D88Image& img, std::vector<DarmFS>& filesystems)
{
	// Update disk sectors with new filesystem
	uint32_t disk = 0;
	for (auto& fs : filesystems)
	{
		if (img.getDiskTitle(disk) == "USER  ")
		{
			disk++;
			continue;
		}
		
		if (fs.isEdited())
		{
			fs.save(img, disk);
		}
		disk++;
	}
	
	// TODO: Handle error
	
	// Write D88 image
	std::vector<uint8_t> data;
	img.saveToBytes(data);

	return writeFile(path, &data[0], data.size());
}

static std::vector<DarmFS> gFilesystems;
static uint32_t gDisk;

static bool loadD88File(D88Image& img, const std::filesystem::path& path)
{
    std::vector<uint8_t> fileData;
    if (!readFile(path, fileData))
    {
        return false;
    }
    
    // Parse disk iamge and filesystem
    img.loadData(&fileData[0], fileData.size());
    gDisk = 0;
    for (int i=0; i<img.getDiskCount(); i++)
    {
        gFilesystems.emplace_back();
        if (img.getDiskTitle(i) == "USER  ")
        {
            continue;
        }
        gFilesystems[i].load(img, i);
    }
    
    return true;
}

//=============================================================================
//============================================================================= 
//=============================================================================

#if !defined(_DARMKEY_CMDONLY_)
	static nana::listbox gLst;
	static nana::checkbox showTextFilesOnlyCheckbox;

	static nana::button loadD88Button;
	static nana::button saveD88Button;
	static nana::button extractAllButton;
	static nana::button extractButton;
	static nana::button insertUpdateButton;
	static nana::button deleteButton;
	static nana::button alterMagicButton;

	static nana::button exportTextJPButton;
	static nana::button exportTextENButton;
	static nana::button importTextButton;

	//=============================================================================
	//============================================================================= 
	//=============================================================================

	struct EntryRow
	{
		std::string mName;
		uint32_t mSize;
		std::string mChanged;
	};

	static void lstMarkRow(uint32_t disk, const std::string& str)
	{
		if (disk >= gFilesystems.size()) return;
		auto& fs = gFilesystems[disk];
		
		const DarmFS::Entry* entry = fs.getFile(str.c_str());
		if (entry == nullptr) return;
		for (auto item : gLst.at(0))
		{
			if (strcmp(item.text(0).c_str(), str.c_str()) == 0)
			{
				item.text(1, std::to_wstring(entry->mData.size()));
				item.text(2, "*");
				item.bgcolor(nana::color(nana::colors::blanched_almond));
				break;
			}
		}
	}

	bool updateFilesystemFile(uint32_t disk, const char* filename, const std::vector<uint8_t>& filedata)
	{
		if (disk >= gFilesystems.size()) return false;
		auto& fs = gFilesystems[disk];
		
		// Update file
		bool newFile  = (fs.getFile(filename) == nullptr);
		DarmFS::Entry* entry = fs.writeFile(filename, &filedata[0], filedata.size());
		
		if ((disk == gDisk) && (entry != nullptr))
		{
			if (newFile)
			{
				auto cat = gLst.at(0);
				EntryRow row;
				row.mName = entry->mName;
				row.mSize = entry->mData.size();
				cat.append(row);
			}
			
			// Mark row
			lstMarkRow(disk, entry->mName);
			
			return true;
		}
		
		return false;
	}

	static void updateListUI() {
		
		extractButton.enabled(false);
		deleteButton.enabled(false);
		alterMagicButton.enabled(false);
		
		if (gDisk >= gFilesystems.size())
		{
			gLst.clear();
			return;
		}
		auto& fs = gFilesystems[gDisk];
		
		bool onlyShowText = showTextFilesOnlyCheckbox.checked();
		AlterMagic::YsDisk ysDisk = AlterMagic::detectYsDisk(fs);
		int ysNumber = AlterMagic::getYsNumber(ysDisk);
		
		gLst.auto_draw(false);
		gLst.clear();
		auto cat = gLst.at(0);
		for (const DarmFS::Entry entry : fs)
		{
			if (onlyShowText && (AlterMagic::getYsFileType(ysNumber, entry.mName) == AlterMagic::FILETYPE_NONE))
			{
				// We're only showing text files and this isn't a recognized text file
				continue;
			}
			
			EntryRow row;
			row.mName = entry.mName;
			row.mSize = entry.mData.size();
			row.mChanged = entry.mEdited ? "*" : "";
			cat.append(row);
		}
		gLst.column_at(0).fit_content();
		gLst.column_at(1).fit_content();
		gLst.column_at(2).fit_content();
		gLst.auto_draw(true);
		
		saveD88Button.enabled(true);
	}

	static nana::listbox::oresolver& operator<<(nana::listbox::oresolver& ores, const EntryRow& entry)
	{
		return ores<<entry.mName<<entry.mSize<<entry.mChanged;
	}

	static bool listbox_padded_sort_compare(const std::string& s1, nana::any*, const std::string& s2, nana::any*, bool reverse)
	{
		if (s1.length() != s2.length())
		{
			return (reverse ? s1.length() > s2.length() : s1.length() < s2.length());
		}
		
		return (reverse ? s1 > s2 : s1 < s2);
	}

	static int mainGui(D88Image& img)
	{
		using namespace nana;

		// Declare form
		form fm(API::make_center(350, 400));
		
		// Set form title
		fm.caption("Darm Key");

		//Define a label and display a text.
		gLst.create(fm);
		gLst.sortable(false);
		gLst.column_movable(false);
		nana::parameters::mouse_wheel argMouseWheel;
		gLst.scheme().mouse_wheel.lines = 1;
		gLst.append_header("File");
		gLst.append_header("Size");
		gLst.append_header("Changed");
		gLst.set_sort_compare(1, listbox_padded_sort_compare);

		label fileLabel{fm, "No File"};
		fileLabel.text_align(nana::align::center, nana::align_v::center);
		
		showTextFilesOnlyCheckbox.create(fm);
		showTextFilesOnlyCheckbox.caption("Show only recognized text files");
		showTextFilesOnlyCheckbox.check(false);
		
		loadD88Button.create(fm); loadD88Button.caption("Load D88");
		saveD88Button.create(fm); saveD88Button.caption("Save As");
		saveD88Button.enabled(false);
		extractAllButton.create(fm); extractAllButton.caption("Extract All");
		extractButton.create(fm); extractButton.caption("Extract");
		extractButton.enabled(false);
		insertUpdateButton.create(fm); insertUpdateButton.caption("Insert/Update");
		deleteButton.create(fm); deleteButton.caption("Delete");
		deleteButton.enabled(false);
		alterMagicButton.create(fm); alterMagicButton.caption("Alter Magic");
		alterMagicButton.enabled(false);

		exportTextJPButton.create(fm); exportTextJPButton.caption("Export All Text (JP)");
		exportTextENButton.create(fm); exportTextENButton.caption("Export All Text (EN)");
		importTextButton.create(fm); importTextButton.caption("Import Text");

		filebox openFBox(fm, true);
		openFBox.allow_multi_select(false);
		openFBox.add_filter("D88 File", "*.d88");
		openFBox.add_filter("All Files", "*.*");

		filebox saveFBox(fm, false);
		saveFBox.allow_multi_select(false);
		saveFBox.add_filter("D88 File", "*.d88");
		saveFBox.add_filter("All Files", "*.*");
		
		folderbox extractFolderFBox(fm);
		extractFolderFBox.allow_multi_select(false);
		
		filebox extractSingleFBox(fm, false);
		extractSingleFBox.allow_multi_select(false);
		extractSingleFBox.add_filter("All Files", "*");
		
		filebox insertFBox(fm, true);
		insertFBox.allow_multi_select(false);
		insertFBox.add_filter("All Files", "*");
		
		label diskSelectLabel(fm);
		diskSelectLabel.caption("Disk: ");
		
		combox diskSelect(fm);
		diskSelect.editable(false);

		if (img.getDiskCount() > 0)
		{
			diskSelect.clear();
			for (int i=0; i<img.getDiskCount(); i++)
			{
				diskSelect.push_back(img.getDiskTitle(i));
			}
			diskSelect.option(gDisk);
			updateListUI();
		}
		
		// Selected a new disk
		diskSelect.events().selected([&diskSelect]{
			gDisk = diskSelect.option();
			updateListUI();
		});

		showTextFilesOnlyCheckbox.events().checked([]{
			updateListUI();
		});
		
		// Load D88 Button Click
		loadD88Button.events().click([&openFBox, &fileLabel, &img, &diskSelect]{
			auto files = openFBox.show();
			if (!files.empty())
			{
				if (!loadD88File(img, files[0]))
				{
					msgbox mb("Error");
					mb << "Couldn't read file";
					mb.show();
					return;
				}
				
				// Parse disk iamge and filesystem
				gDisk = 0;
				diskSelect.clear();
				for (int i=0; i<img.getDiskCount(); i++)
				{
					diskSelect.push_back(img.getDiskTitle(i));
				}
				diskSelect.option(gDisk);

				// Update UI
				fileLabel.caption(files[0].filename());
				
				updateListUI();
			}
		});
		
		// Save As Button Click
		saveD88Button.events().click([&saveFBox, &img, &fileLabel]{
			auto files = saveFBox.show();
			if (!files.empty())
			{
				saveFilesystem(files[0], img, gFilesystems);
				
				fileLabel.caption(files[0].filename());
				updateListUI();
			}
		});
		
		// Extract All Button Click
		extractAllButton.events().click([&extractFolderFBox]{
			if (gDisk >= gFilesystems.size()) return;
			auto& fs = gFilesystems[gDisk];
		
			extractFolderFBox.title("Extract All Files");
			auto paths = extractFolderFBox.show();
			if (!paths.empty())
			{
				for (const DarmFS::Entry entry : fs)
				{
					std::filesystem::path filePath = paths[0];
					filePath /= entry.mName;
					writeFile(filePath, &entry.mData[0], entry.mData.size());
				}
			}
		});
		
		// Extract Button Click
		extractButton.events().click([&extractFolderFBox, &extractSingleFBox]{
			if (gDisk >= gFilesystems.size()) return;
			auto& fs = gFilesystems[gDisk];
			
			// Extract files...
			auto selected = gLst.selected();
			if (selected.size() > 1)
			{	
				// Multiple selected, get destination dir
				extractFolderFBox.title("Extract Multiple Files");
				auto paths = extractFolderFBox.show();
				if (!paths.empty())
				{
					for (auto item : selected)
					{
						std::string name = gLst.at(item).text(0);
						const DarmFS::Entry* entry = fs.getFile(name.c_str());
						if (entry != nullptr)
						{
							std::filesystem::path filePath = paths[0];
							filePath /= entry->mName;
							writeFile(filePath, &entry->mData[0], entry->mData.size());
						}
					}
				}
			}
			else if (selected.size() == 1)
			{
				std::string name = gLst.at(selected[0]).text(0);
				const DarmFS::Entry* entry = fs.getFile(name.c_str());
				if (entry != nullptr)
				{			
					// One item selected
					extractSingleFBox.title("Extract File");
					extractSingleFBox.init_file(name);
					auto files = extractSingleFBox.show();
					if (!files.empty())
					{
						writeFile(files[0], &entry->mData[0], entry->mData.size());
					}
				}
			}
		});
		
		// Insert/Update Button Click
		insertUpdateButton.events().click([&insertFBox]{
			if (gDisk >= gFilesystems.size()) return;
			auto& fs = gFilesystems[gDisk];
			
			// Insert/Update files...
			insertFBox.title("Import/Update File");
			auto files = insertFBox.show();
			for (auto file : files)
			{
				std::vector<uint8_t> fileData;
				if (!readFile(file, fileData))
				{
					msgbox mb("Error");
					mb << "Couldn't read " << file.filename();
					mb.show();
					continue;
				}
				
				// Update file
				bool newFile  = (fs.getFile(file.filename().u8string().c_str()) == nullptr);
				DarmFS::Entry* entry = fs.writeFile(file.filename().u8string().c_str(), &fileData[0], fileData.size());
				
				if (entry != nullptr)
				{
					if (newFile)
					{
						auto cat = gLst.at(0);
						EntryRow row;
						row.mName = entry->mName;
						row.mSize = entry->mData.size();
						cat.append(row);
					}
					
					// Mark row
					for (auto item : gLst.at(0))
					{
						if (strcmp(item.text(0).c_str(), entry->mName) == 0)
						{
							item.text(1, std::to_wstring(entry->mData.size()));
							item.text(2, "*");
							item.bgcolor(color(colors::blanched_almond));
							break;
						}
					}
				}
			}
		});
			
		// Extract Button Click
		deleteButton.events().click([]{
			if (gDisk >= gFilesystems.size()) return;
			auto& fs = gFilesystems[gDisk];
			
			// Extract files...
			while (gLst.selected().size() > 0)
			{
				auto item = gLst.at(gLst.selected()[0]);
				
				std::string name = item.text(0);
				if (fs.deleteFile(name.c_str()))
				{
					gLst.erase(item);
				}
				else
				{
					item.select(false);
				}
			}
		});

		// AlterMagic Button Click
		AlterMagic::AlterMagicUI altermagic(fm);
		alterMagicButton.events().click([&altermagic]{
			if (gDisk >= gFilesystems.size()) return;
			auto& fs = gFilesystems[gDisk];
			
			if (gLst.selected().size() > 0)
			{
				auto item = gLst.at(gLst.selected()[0]);
				std::string filename = item.text(0);
				altermagic.show(fs, gDisk, filename);
			}
		});
		
		// Export All Text (JP) Click
		exportTextJPButton.events().click([&extractFolderFBox]{
			if (gDisk >= gFilesystems.size()) return;
			auto& fs = gFilesystems[gDisk];
		
			extractFolderFBox.title("Export All Text (JP)");
			auto paths = extractFolderFBox.show();
			if (!paths.empty())
			{
				std::filesystem::path filePath = paths[0];
				std::vector<std::string> fileList = AlterMagic::TextFile::exportAllText(fs, paths[0], false);
				msgbox mb("Export All Text (JP)");
				mb << generateExportTextMessage(fileList);
				mb.show();
			}
		});
		
		// Export All Text (EN) Click
		exportTextENButton.events().click([&extractFolderFBox]{
			if (gDisk >= gFilesystems.size()) return;
			auto& fs = gFilesystems[gDisk];
		
			extractFolderFBox.title("Export All Text (EN)");
			auto paths = extractFolderFBox.show();
			if (!paths.empty())
			{
				std::vector<std::string> fileList = AlterMagic::TextFile::exportAllText(fs, paths[0], true);
				msgbox mb("Export All Text (EN)");
				mb << generateExportTextMessage(fileList);
				mb.show();
			}
		});
		
		// Import Text Click
		importTextButton.events().click([&extractFolderFBox]{
			if (gDisk >= gFilesystems.size()) return;
			auto& fs = gFilesystems[gDisk];
		
			extractFolderFBox.title("Import Text");
			auto paths = extractFolderFBox.show();
			if (!paths.empty())
			{
				std::vector<std::string> fileList = AlterMagic::TextFile::importAllText(fs, paths[0]);
				for (const std::string& updatedFile : fileList)
				{
					lstMarkRow(gDisk, updatedFile);
				}
				msgbox mb("Import Text");
				mb << generateImportTextMessage(fileList);
				mb.show();
			}
		});
		
		gLst.events().selected([]{
			bool anythingSelected = gLst.selected().size() > 0;
			extractButton.enabled(anythingSelected);
			deleteButton.enabled(anythingSelected);
			alterMagicButton.enabled(anythingSelected);
		});
		
		//Layout management
		fm.div("vert<weight=24 <fit loadD88Button><fit saveD88Button><fileLabel>><weight=24 <fit diskSelectLabel><diskSelect>><list><weight=24 <fit checkboxRow>><weight=24 <fit bottomButtons>><weight=24 <fit bottomButtons2>>");
		fm["loadD88Button"] << loadD88Button;
		fm["saveD88Button"] << saveD88Button;
		fm["fileLabel"]<< fileLabel;
		fm["diskSelectLabel"]<< diskSelectLabel;
		fm["diskSelect"]<< diskSelect;
		fm["list"]<<gLst;
		fm["checkboxRow"] << showTextFilesOnlyCheckbox;
		fm["bottomButtons"] << extractAllButton << extractButton << insertUpdateButton << deleteButton << alterMagicButton;
		fm["bottomButtons2"] << exportTextJPButton << exportTextENButton << importTextButton;
		fm.collocate();
		
		//Show the form
		fm.show();

		//Start to event loop process, it blocks until the form is closed.
		exec();
		
		return 0;
	}
#endif // !defined(_DARMKEY_CMDONLY_)

int main(int argc, const char* argv[])
{
    namespace prog_opts = boost::program_options;

	bool overwriteFlag = false;
	bool textOnlyFlag = false;

    // Define arguments
    prog_opts::options_description desc("The following command line options are supported");
    desc.add_options()
        ("help,h", "describes command line options")
        ("file,f", prog_opts::value<std::string>(), "d88 disk image file to use")
        ("disk,d", prog_opts::value<int>(), "disk index to use within the file")
        ("extract,x", prog_opts::value<std::string>(), "extract the specified file")
        ("output,o", prog_opts::value<std::string>(), "output path to extract to")
		("insert,i", prog_opts::value<std::string>(), "insert/update a specified file")
        ("write-file,w", prog_opts::value<std::string>(), "output path to write the modified copy d88 file to, defaulting to the input file with .NEW appended to the filename")
		("overwrite", prog_opts::bool_switch(&overwriteFlag), "overwrite the d88 file we opened from")
		("export-text-en", prog_opts::value<std::string>(), "exports all text files to the specified directory, assuming EN encoding")
		("export-text-jp", prog_opts::value<std::string>(), "exports all text files to the specified directory, assuming JP encoding")
		("import-text", prog_opts::value<std::string>(), "imports all text files from the specified directory")
		("extract-all", prog_opts::value<std::string>(), "extracts all files to the specified path")
		("text-only", prog_opts::bool_switch(&textOnlyFlag), "makes --extract-all only export recognized text files")
		("update-all", prog_opts::value<std::string>(), "updates all files from copies in the specified path")
    ;

    // Parse arguments
    prog_opts::variables_map vm;
    prog_opts::store(prog_opts::parse_command_line(argc, argv, desc), vm);
    prog_opts::notify(vm); 

    // Help message
    if (vm.count("help")) {
        std::cout << desc << "\n";
        return 0;
    }

	// Declare data storage	
	D88Image img;
	int diskCount = 0;

	std::string d88Filename;
    if (vm.count("file")) {
		d88Filename = vm["file"].as<std::string>();
        if (!loadD88File(img, d88Filename))
        {
            std::cout << "Couldn't read " << d88Filename << "\n";
            return 1;
        }
		diskCount = img.getDiskCount();
        
        if (vm.count("disk")) {
            int diskIdx = vm["disk"].as<int>();
            if ((diskIdx < 1) || (diskIdx > diskCount))
            {
                std::cout << "No such disk '" << diskIdx << "'\n";
                return 1;
            }
            gDisk = diskIdx - 1;
        } else if (diskCount > 1) {
			std::cout << "The d88 file contains multiple disks. Please specify a disk index from 1 to " << diskCount << "\n";
			return 1;
		}
		else
		{
			gDisk = 0;
		}
    }
    
    if (vm.count("extract"))
	{
        std::string extractFile = vm["extract"].as<std::string>();
        std::string outputFile = extractFile;
        
        if (vm.count("output"))
		{
            std::string outputPath = vm["output"].as<std::string>();
			char lastChar = (outputPath.size() > 0) ? outputPath[outputPath.size()-1] : '\0';
			if (lastChar == '/' || lastChar == '\\')
			{
				// Case of obvious directory as path
				outputFile = outputPath + outputFile;
			}
			else if (std::filesystem::is_directory(outputPath))
			{
				// Is it a directory that actually exists?
				std::filesystem::path outputPathObj = outputPath;
				outputPathObj /= outputFile;
				outputFile = outputPathObj.u8string();
			}
			else
			{
				// Otherwise must be full filename itself
				outputFile = outputPath;
			}
        }
        
        if (gDisk >= diskCount)
        {
            std::cout << "Can't extract, no disk open.\n";
            return 1;
        }
        
        auto& fs = gFilesystems[gDisk];
        const DarmFS::Entry* entry = fs.getFile(extractFile.c_str());
        if (entry == nullptr)
        {
            std::cout << "Can't extract, no such file \"" << extractFile << "\".\n";
            return 1;
        }
        
        if (!writeFile(outputFile, &entry->mData[0], entry->mData.size()))
        {
            std::cout << "Writing \"" << outputFile << "\" failed.\n";
            return 1;
        }
		else
		{
			std::cout << "Wrote \"" << outputFile << "\" to disk.\n";
		}
        
        return 0;
    }
    
    if (vm.count("insert")) {
        std::filesystem::path inputFile = vm["insert"].as<std::string>();
        std::string updateFile = inputFile.stem().u8string();
		std::transform(updateFile.begin(), updateFile.end(),updateFile.begin(), ::toupper);
        std::string d88OutFilename = d88Filename + ".NEW";
        
        // check write-file arg
		if (overwriteFlag) {
			d88OutFilename = d88Filename;
		}
        else if (vm.count("write-file")) {
            d88OutFilename = vm["write-file"].as<std::string>();
        }
        
        // Check disk index
        if (gDisk >= diskCount)
        {
            std::cout << "Can't insert/update, no disk open.\n";
            return 1;
        }
        
        // Attempt to read update file
        std::vector<uint8_t> fileData;
        if (!readFile(inputFile, fileData))
        {
            std::cout << "Reading \"" << inputFile << "\" failed.\n";
            return 1;
        }
			
        // Update file
        auto& fs = gFilesystems[gDisk];
        DarmFS::Entry* entry = fs.writeFile(updateFile.c_str(), &fileData[0], fileData.size());
		if (entry == nullptr)
        {
            std::cout << "Updating \"" << updateFile << "\" failed.\n";
            return 1;
        }
        
        // Try to write filesystem to disk image
        if (!saveFilesystem(d88OutFilename, img, gFilesystems))
        {
            std::cout << "Writing \"" << d88OutFilename << "\" failed.\n";
            return 1;
        }
		else
		{
			std::cout << "Wrote \"" << d88OutFilename << "\" to disk.\n";
		}
        
        return 0;
    }
    
	if (vm.count("export-text-en") || vm.count("export-text-jp")) {
        std::filesystem::path outputPath = vm.count("export-text-en") ? vm["export-text-en"].as<std::string>() : vm["export-text-jp"].as<std::string>();
        
        // Check disk index
        if (gDisk >= diskCount)
        {
            std::cout << "No disk specified.\n";
            return 1;
        }
		
		// See if the output path exists
		if (std::filesystem::is_directory(outputPath))
		{
			// Output directory exists
		}
		else if (std::filesystem::create_directories(outputPath))
		{
			// Successfully created directory
			std::cout << "Created directory \"" << outputPath.string() << "\".\n";
		}
		else
		{
			// Directory didn't already exist and could not create it
			std::cout << "Could not create directory \"" << outputPath.string() << "\".\n";
			return 1;
		}
        
		auto& fs = gFilesystems[gDisk];
		std::vector<std::string> fileList = AlterMagic::TextFile::exportAllText(fs, outputPath, (vm.count("export-text-en") != 0));
		std::cout << generateExportTextMessage(fileList) << "\n";
		return 0;
	}
	
	if (vm.count("import-text")) {
        std::filesystem::path inputPath = vm["import-text"].as<std::string>();
		std::string d88OutFilename = d88Filename + ".NEW";
        
        // Check disk index
        if (gDisk >= diskCount)
        {
            std::cout << "No disk specified.\n";
            return 1;
        }
		
		// See if the input path exists
		if (!std::filesystem::is_directory(inputPath))
		{
			std::cout << "The directory \"" << inputPath.string() <<  "\" does not exist.\n";
			return 1;
		}
		
        // check write-file arg
		if (overwriteFlag) {
			d88OutFilename = d88Filename;
		}
        else if (vm.count("write-file")) {
            d88OutFilename = vm["write-file"].as<std::string>();
        }
		
		auto& fs = gFilesystems[gDisk];
		std::vector<std::string> fileList = AlterMagic::TextFile::importAllText(fs, inputPath);
		std::cout << generateImportTextMessage(fileList) << "\n";
		
        // Try to write filesystem to disk image
        if (!saveFilesystem(d88OutFilename, img, gFilesystems))
        {
            std::cout << "Writing \"" << d88OutFilename << "\" failed.\n";
            return 1;
        }
		else
		{
			std::cout << "Wrote \"" << d88OutFilename << "\" to disk.\n";
		}
		return 0;
	}
	
	if (vm.count("extract-all")) {
        std::filesystem::path outputPath = vm["extract-all"].as<std::string>();
		
        // Check disk index
        if (gDisk >= diskCount)
        {
            std::cout << "No disk specified.\n";
            return 1;
        }
		
		// See if the output path exists
		if (std::filesystem::is_directory(outputPath))
		{
			// Output directory exists
		}
		else if (std::filesystem::create_directories(outputPath))
		{
			// Successfully created directory
			std::cout << "Created directory \"" << outputPath.string() << "\".\n";
		}
		else
		{
			// Directory didn't already exist and could not create it
			std::cout << "Could not create directory \"" << outputPath.string() << "\".\n";
			return 1;
		}
		
		auto& fs = gFilesystems[gDisk];
		AlterMagic::YsDisk ysDisk = AlterMagic::detectYsDisk(fs);
		int ysNumber = AlterMagic::getYsNumber(ysDisk);
		
		for (auto it = fs.cbegin(); it != fs.cend(); it++)
		{
			const DarmFS::Entry& entry = *it;
			
			// Load as text file if possible
			if (textOnlyFlag && (AlterMagic::getYsFileType(ysNumber, entry.mName) == AlterMagic::FILETYPE_NONE))
			{
				// We're only extracting text files and this isn't a recognized text file
				continue;
			}
			
			// Get path
			std::string fileName = std::string(entry.mName);
			std::filesystem::path filePath = outputPath;
			filePath /= fileName;
			
			// Extract to file
			writeFile(filePath, &entry.mData[0], entry.mData.size());
			
			// Give message
			std::cout << "Wrote " << filePath.string() << "\n";
		}
		return 0;
	}
	
	if (vm.count("update-all")) {
        std::filesystem::path inputPath = vm["update-all"].as<std::string>();
		std::string d88OutFilename = d88Filename + ".NEW";
        
        // Check disk index
        if (gDisk >= diskCount)
        {
            std::cout << "No disk specified.\n";
            return 1;
        }
		
		// See if the input path exists
		if (!std::filesystem::is_directory(inputPath))
		{
			std::cout << "The directory \"" << inputPath.string() <<  "\" does not exist.\n";
			return 1;
		}
		
        // check write-file arg
		if (overwriteFlag) {
			d88OutFilename = d88Filename;
		}
        else if (vm.count("write-file")) {
            d88OutFilename = vm["write-file"].as<std::string>();
        }
		
		auto& fs = gFilesystems[gDisk];
		std::vector<std::string> fileList;
		std::vector<std::string> diskFiles;
		for (auto it = fs.cbegin(); it != fs.cend(); it++)
		{
			const DarmFS::Entry& entry = *it;
			diskFiles.push_back(entry.mName);
		}
		
		for (const std::string& fn : diskFiles)
		{
			// Get path
			std::string fileName = fn;
			std::filesystem::path filePath = inputPath;
			filePath /= fileName;
			
			// Try to read file
			std::vector<uint8_t> fileData;
			if (!readFile(filePath, fileData))
			{
				// Can't read file
				continue;
			}
			
			// Pad to even size
			fileData.resize(fileData.size() + (fileData.size() % 2), 0xFF); 
			
			// Check if the file has changed
			const DarmFS::Entry* entry = fs.getFile(fn.c_str());
			if ((entry == nullptr) || (fileData == entry->mData))
			{
				// File unchanged
				continue;
			}
			
			// Update data
			fs.writeFile(fn.c_str(), &fileData[0], fileData.size());
			
			// Give message
			std::cout << "Updated " << fileName << "\n";
		}
		
        // Try to write filesystem to disk image
        if (!saveFilesystem(d88OutFilename, img, gFilesystems))
        {
            std::cout << "Writing \"" << d88OutFilename << "\" failed.\n";
            return 1;
        }
		else
		{
			std::cout << "Wrote \"" << d88OutFilename << "\" to disk.\n";
		}
		return 0;
	}
	
	#if defined(_DARMKEY_CMDONLY_)
		std::cout << "No operation specified. Call with -h for help.\n";
		return 1;
	#else
		return mainGui(img);
	#endif
}
