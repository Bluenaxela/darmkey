#include <cstdint>
#include <cstdio>
#include <cstring>
#include "d88.h"

// NOTE: This file assumes the image file matches native endian!

struct D88Header {
	char     title[17];
	uint8_t  reserved[9];
	uint8_t  protect;
	uint8_t  type;
	uint32_t size;
	uint32_t trackPtrs[164];
};

struct D88SectorHeader {
	uint8_t  track;
	uint8_t  side;
	uint8_t  sector;
	uint8_t  sectorSize;
	uint16_t numSectors;
	uint8_t  density;
	uint8_t  del;
	uint8_t  stat;
	uint8_t  reserved[5];
	uint16_t size;
};


D88Image::D88Image() :
	mDisks(), mValid(false)
{
}

D88Image::~D88Image()
{
}

void D88Image::loadData(const uint8_t* data, uint32_t size)
{
	mValid = false;
	mDisks.clear();

	uint32_t diskIdx = 0;
	uint32_t diskOffset = 0; // Index we're at measured in terms of the disk
	while (diskOffset + sizeof(D88Header) <= size)
	{
		// Get disk data pointer;
		const uint8_t* diskData = &data[diskOffset];
		const D88Header& header = *(const D88Header*)&diskData[0];
		
		// Add disk to list...
		mDisks.emplace_back();
		auto& disk = mDisks.back();
		if (header.title[0] != '\0') {
			// Use title from file
			char tmpTitle[17];
			memcpy(tmpTitle, header.title, 16);
			tmpTitle[16] = '\0';
			disk.title = tmpTitle;
		}
		else
		{
			// No title in file, let's make our own
			disk.title = "DISK " + std::to_string(diskIdx + 1);
		}
		
		// Tracks array
		auto& tracks = disk.tracks;
		
		for (int i = 0; i < 164; i++)
		{
			uint32_t cursor = header.trackPtrs[i];
			if (cursor == 0) break;
			if (cursor + sizeof(D88SectorHeader) > size) break;
			const D88SectorHeader& trackHeader = *(const D88SectorHeader*)&diskData[cursor];
			
			while (trackHeader.track >= tracks.size())
			{
				tracks.emplace_back(); // Add track
			}
			sidelist_t& sides = tracks[trackHeader.track];
			while (trackHeader.side >= sides.size())
			{
				sides.emplace_back(); // Add side
			}
			sectorlist_t& sectors = sides[trackHeader.side];
			
			while (cursor < size)
			{
				const D88SectorHeader& sectorHeader = *(const D88SectorHeader*)&diskData[cursor];
				if ((sectorHeader.track != trackHeader.track) || (sectorHeader.side != trackHeader.side))
				{
					printf("Break Because mismatch (diskIdx=%u sectorHeader.track=%u trackHeader.track=%u, sectorHeader.side=%u trackHeader.side=%u)\n",
						diskIdx,
						sectorHeader.track, trackHeader.track,
						sectorHeader.side, trackHeader.side
						);
					break;
				}
				
				cursor += sizeof(D88SectorHeader);
				if (sectorHeader.sector == 0)
				{
					// This shouldn't happen because sectors seem to be numbered starting from 1? Let's skip this sector.
					cursor += sectorHeader.size;
					continue;
				}
				
				while ((sectorHeader.sector - 1) >= sectors.size())
				{
					sectors.emplace_back(); // Add sector
				}
				sector_t& imgSector = sectors[sectorHeader.sector - 1];
				
				imgSector.resize(sectorHeader.size);
				memcpy(&imgSector[0], &diskData[cursor], sectorHeader.size);
				cursor += sectorHeader.size;
				
				if (sectors.size() >= sectorHeader.numSectors)
				{
					break;
				}
			}
		}
		
		diskIdx++;
		diskOffset += header.size;
	}
	
	mValid = mDisks.size() > 0;
}

void D88Image::saveToBytes(std::vector<uint8_t>& out)
{
	out.clear();
	
	uint32_t diskOffset = 0;
	for (auto& disk : mDisks)
	{
		{
			// Add disk header with title
			out.resize(diskOffset + sizeof(D88Header));
			D88Header& header = *(D88Header*)&out[diskOffset];
			strncpy(header.title, disk.title.c_str(), 16);
			header.title[16] = '\0';
		}
		
		// Tracks array
		auto& tracks = disk.tracks;
		
		// Loop over all of the tracks
		for (uint32_t track = 0; track < tracks.size(); track++)
		{
			sidelist_t& sides = tracks[track];
			for (uint32_t side = 0; side < sides.size(); side++)
			{
				sectorlist_t& sectors = sides[side];
				
				// Update pointer in header
				{
					D88Header& header = *(D88Header*)&out[diskOffset];
					header.trackPtrs[track*2 + side] = out.size() - diskOffset;
				}
				
				for (uint32_t sector = 1; sector <= sectors.size(); sector++)
				{
					sector_t& sectorData = sectors[sector-1];
					uint32_t sectorHeaderOffset = out.size() - diskOffset;
					uint32_t sectorDataOffset = sectorHeaderOffset + sizeof(D88SectorHeader);
					uint32_t sectorDataSize = sectorData.size();
					out.resize(diskOffset + sectorDataOffset + sectorDataSize);
					
					D88SectorHeader& sectorHeader = *(D88SectorHeader*)&out[diskOffset + sectorHeaderOffset];
					sectorHeader.track = track;
					sectorHeader.side = side;
					sectorHeader.sector = sector;
					
					if (sectorDataSize <= 128)
					{
						sectorHeader.sectorSize = 0;
					}
					else if (sectorDataSize <= 256)
					{
						sectorHeader.sectorSize = 1;
					}
					else if (sectorDataSize <= 512)
					{
						sectorHeader.sectorSize = 2;
					}
					else
					{
						sectorHeader.sectorSize = 3;
					}
					
					sectorHeader.numSectors = sectors.size();
					sectorHeader.density = 0;
					sectorHeader.del = 0;
					sectorHeader.stat = 0;
					
					sectorHeader.size = sectorDataSize;
					memcpy(&out[diskOffset + sectorDataOffset], &sectorData[0], sectorDataSize);
				}
			}
		}
		
		// Update total size
		{
			D88Header& header = *(D88Header*)&out[diskOffset];
			header.size = out.size() - diskOffset;
			diskOffset = out.size();
		}
	}
}

const uint8_t* D88Image::getSector(uint32_t disk, uint8_t track, uint8_t side, uint8_t sector, uint16_t& sizeOut)
{
	if (disk >= mDisks.size()) return nullptr;
	tracklist_t& tracks = mDisks[disk].tracks;
	if (track >= tracks.size()) return nullptr;
	sidelist_t& sides = tracks[track];
	if (side >= sides.size()) return nullptr;
	sectorlist_t& sectors = sides[side];
	if ((sector < 1) || (sector > sectors.size())) return nullptr;
	sector_t& sectorData = sectors[sector - 1];
	
	sizeOut = sectorData.size();
	return &sectorData[0];
}

void D88Image::copyRange(uint32_t disk, uint8_t track, uint8_t side, uint8_t sector, uint8_t endTrack, uint8_t endSide, uint8_t endSector, std::vector<uint8_t>& out)
{
	if (disk >= mDisks.size()) return;
	tracklist_t& tracks = mDisks[disk].tracks;
	
	uint32_t cursor = 0;
	while (!(track > endTrack) && !(track == endTrack && side > endSide) && !(track == endTrack && side == endSide && sector > endSector))
	{
		uint16_t sectorSize;
		const uint8_t* sectorData = getSector(disk, track, side, sector, sectorSize);
		if (sectorData == nullptr) return;
		
		out.resize(out.size() + sectorSize);
		memcpy(&out[cursor], sectorData, sectorSize);
		cursor += sectorSize;
		
		uint16_t numSectors;
		{
			if (track >= tracks.size()) return;
			sidelist_t& sides = tracks[track];
			if (side >= sides.size()) return;
			sectorlist_t& sectors = sides[side];
			numSectors = sectors.size();
		}
		
		if (sector < numSectors)
		{
			sector++;
		}
		else if (side < 1)
		{
			side++;
			sector = 1;
		}
		else
		{
			track++;
			side = 0;
			sector = 1;
		}
	}
}

bool D88Image::writeData(uint32_t disk, uint8_t& track, uint8_t& side, uint8_t& sector, uint8_t& endTrack, uint8_t& endSide, uint8_t& endSector, const uint8_t* data, uint32_t size, uint8_t pad)
{
	if (disk >= mDisks.size()) return false;
	tracklist_t& tracks = mDisks[disk].tracks;
	
	endTrack = track;
	endSide = side;
	endSector = sector;
	
	while (size > 0)
	{
		if (track >= tracks.size()) return false;
		sidelist_t& sides = tracks[track];
		if (side >= sides.size()) return false;
		sectorlist_t& sectors = sides[side];
		if ((sector < 1) || (sector > sectors.size())) return false;
		sector_t& sectorData = sectors[sector - 1];
		
		uint32_t sectorSize = sectorData.size();
		if (size < sectorSize)
		{
			// Copy data and pad to sector size
			memcpy(&sectorData[0], data, size);
			memset(&sectorData[size], pad, sectorSize - size);
			size = 0;
		}
		else
		{
			// 
			memcpy(&sectorData[0], data, sectorSize);
			data = &data[sectorSize];
			size -= sectorSize;
		}
		
		// Exit if we're done
		if (size == 0)
		{
			endTrack = track;
			endSide = side;
			endSector = sector;
		}
		
		// Increment indicies
		if (sector < sectors.size())
		{
			sector++;
		}
		else if (side < 1)
		{
			side++;
			sector = 1;
		}
		else
		{
			track++;
			side = 0;
			sector = 1;
		}
	}
	
	return true;
}
