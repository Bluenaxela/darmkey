#include <cstdio>
#include "fs_compat.h"

static FILE* fopen_compat(const std::filesystem::path& path, bool write)
{
	#if defined(WIN32)
		return _wfopen(path.c_str(), write ? L"wb" : L"rb");
	#else
		return std::fopen(path.c_str(), write ? "wb" : "rb");
	#endif
}

bool readFile(const std::filesystem::path& path, std::vector<uint8_t>& out)
{
	out.clear();
	
	FILE* f = fopen_compat(path, false);
	if (f == nullptr) return false;
	
	// Get file size
	if (fseek(f, 0, SEEK_END) != 0)
	{
		fclose(f);
		return false;
	}
	int ret = ftell(f);
	if (ret < 0)
	{
		fclose(f);
		return false;
	}
	if (fseek(f, 0, SEEK_SET) != 0)
	{
		fclose(f);
		return false;
	}
	
	out.resize(ret);
	if (fread(&out[0], ret, 1, f) != 1)
	{
		fclose(f);
		return false;
	}
	
	fclose(f);
	return true;
}

bool writeFile(const std::filesystem::path& path, const uint8_t* data, uint32_t size)
{
	FILE* f = fopen_compat(path, true);
	if (f == nullptr) return false;
	
	if (fwrite(data, size, 1, f) != 1)
	{
		fclose(f);
		return false;
	}
	
	fclose(f);
	return true;
}
