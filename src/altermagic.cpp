#include <vector>
#include <cstdint>
#include <sstream>
#include <locale>
#include <codecvt>
#include <string.h>
#if !defined(_DARMKEY_CMDONLY_)
	#include <nana/gui.hpp>
#else
	#include <iostream>
#endif
#include "altermagic.h"
#include "darmfs.h"
#include "charset_ys2_shiftjis.h"
#include "charset_ys1.h"
#include "fs_compat.h"

using namespace AlterMagic;

std::wstring AlterMagic::decodeString(YsTextEncoding encoding, const std::vector<uint8_t>& s, bool assumeEnFont)
{
	if ((encoding == CHARSET_YS2_SHIFTJIS) || (encoding == CHARSET_YS2_SHIFTJIS_WIDEONLY))
	{
		return CharsetYs2ShiftJIS::decodeYs2String(s, assumeEnFont);
	}
	else if (encoding == CHARSET_YS1_FONT)
	{
		return CharsetYs1Font::decodeString(s, assumeEnFont);
	}
	else
	{
		return L"";
	}
}

void AlterMagic::encodeString(YsTextEncoding encoding, std::vector<uint8_t>& s, const std::wstring& str)
{
	if (encoding == CHARSET_YS2_SHIFTJIS)
	{
		CharsetYs2ShiftJIS::encodeYs2String(s, str, true, true);
	}
	else if (encoding == CHARSET_YS2_SHIFTJIS_WIDEONLY)
	{
		CharsetYs2ShiftJIS::encodeYs2String(s, str, false, false);
	}
	else if (encoding == CHARSET_YS1_FONT)
	{
		CharsetYs1Font::encodeString(s, str);
	}
}

YsDisk AlterMagic::detectYsDisk(const DarmFS& fs)
{
	if (
		(fs.getFile("FONTDT") != nullptr) &&
		(fs.getFile("GOVMUS") != nullptr) &&
		(fs.getFile("INVPRG") != nullptr) &&
		(fs.getFile("MAPD10") != nullptr) &&
		(fs.getFile("CHRD10") != nullptr) &&
		(fs.getFile("MANPR1") != nullptr) &&
		(fs.getFile("DKMON1") != nullptr) &&
		(fs.getFile("STATAS") != nullptr) &&
		(fs.getFile("TITLEP") != nullptr)
		)
	{
		return DISK_YS1_A;
	}
	else if (
		(fs.getFile("FONTDT") != nullptr) &&
		(fs.getFile("ENDPRG") != nullptr) &&
		(fs.getFile("BMESAG") != nullptr) &&
		(fs.getFile("INVPRG") != nullptr) &&
		(fs.getFile("MANPR2") != nullptr) &&
		(fs.getFile("MAPD90") != nullptr) &&
		(fs.getFile("PLYD00") != nullptr) &&
		(fs.getFile("YMUS42") != nullptr) &&
		(fs.getFile("MONDAT") != nullptr)
		)
	{
		return DISK_YS1_B;
	}
	else if (
		(fs.getFile("ENDPRG") != nullptr) &&
		(fs.getFile("EVNMS5") != nullptr) &&
		(fs.getFile("END015") != nullptr) &&
		(fs.getFile("TIT015") != nullptr) &&
		(fs.getFile("TTLMS3") != nullptr) &&
		(fs.getFile("TTLPRG") != nullptr) &&
		(fs.getFile("CHRDAT") != nullptr)
		)
	{
		return DISK_YS2_OPENING;
	}
	else if (
		(fs.getFile("EQPNAM") != nullptr) &&
		(fs.getFile("SCENE1") != nullptr) &&
		(fs.getFile("SHPDT4") != nullptr) &&
		(fs.getFile("Y00MAP") != nullptr) &&
		(fs.getFile("ZDKPRG") != nullptr) &&
		(fs.getFile("YSBKPR") != nullptr) &&
		(fs.getFile("SHPMS1") != nullptr) &&
		(fs.getFile("SCENE2") != nullptr)
		)
	{
		return DISK_YS2_A;
	}
	else if (
		(fs.getFile("EQPNAM") != nullptr) &&
		(fs.getFile("SCENE3") != nullptr) &&
		(fs.getFile("SCENE4") != nullptr) &&
		(fs.getFile("SHPDT7") != nullptr) &&
		(fs.getFile("Y11MAP") != nullptr) &&
		(fs.getFile("YLSTMP") != nullptr) &&
		(fs.getFile("ZDKPRG") != nullptr) &&
		(fs.getFile("Z22MUS") != nullptr)
		)
	{
		return DISK_YS2_B;
	}
	else if ((fs.getFile("A50CHC") != nullptr))
	{
		return DISK_YS3_OPENING;
	}
	else if ((fs.getFile("A00CHC") != nullptr))
	{
		return DISK_YS3_SCENARIO;
	}
	else if ((fs.getFile("A10CHC") != nullptr))
	{
		return DISK_YS3_DATA1;
	}
	else if ((fs.getFile("A30CHC") != nullptr))
	{
		return DISK_YS3_DATA2;
	}
	
	return DISK_UNKNOWN;
}

int AlterMagic::getYsNumber(YsDisk disk)
{
	switch (disk)
	{
		case DISK_YS1_A:
		case DISK_YS1_B:
			return 1;
		case DISK_YS2_OPENING:
		case DISK_YS2_A:
		case DISK_YS2_B:
			return 2;
		case DISK_YS3_OPENING:
		case DISK_YS3_SCENARIO:
		case DISK_YS3_DATA1:
		case DISK_YS3_DATA2:
			return 3;
		case DISK_UNKNOWN:
		default:
			return 0;
	}
}

YsFileType AlterMagic::getYsFileType(int ysNumber, const std::string& filename)
{
	// For CHARSET_YS2_SHIFTJIS see: https://discordapp.com/channels/570537659966029824/570537659966029826/633586952863481878
	
	if (ysNumber == 1)
	{
		if ((filename == "MESSAG") || (filename == "MESS12"))
		{
			return FILETYPE_YS1_MESSAG;
		}
		else if (strncmp(filename.c_str(), "SHPMS", 5) == 0)
		{
			return FILETYPE_YS1_SHPMS;
		}
		else if (filename == "BMESAG")
		{
			return FILETYPE_YS1_BMESAG;
		}
	}
	else if (ysNumber == 2)
	{
		if (strncmp(filename.c_str(), "SHPMS", 5) == 0)
		{
			return FILETYPE_YS2_SHPMS;
		}
		else if (filename == "EQPNAM")
		{
			return FILETYPE_YS2_EQPNAM;
		}
		else if (strncmp(filename.c_str(), "EVNMS", 5) == 0)
		{
            return FILETYPE_YS2_EVNMS;
		}
		else if (filename == "YSBKMS")
		{
			return FILETYPE_YS2_YSBKMS;
		}
		else if (strncmp(filename.c_str(), "MESCT", 5) == 0)
		{
			return FILETYPE_YS2_MESCT;
		}
		else if (filename == "TTLPRG")
		{
			return FILETYPE_YS2_TTLPRG;
		}
		else if (filename == "ZDKPRG")
		{
			return FILETYPE_YS2_ZDKPRG;
		}
		else if (filename == "MANPR1")
		{
			return FILETYPE_YS2_MANPR1;
		}
		else if (strncmp(filename.c_str(), "ZDKPR", 5) == 0)
		{
			return FILETYPE_YS2_ZDKPR;
		}
		else if (filename == "EQPIPR")
		{
			return FILETYPE_YS2_PRG_B800;
		}
		else if (filename == "EQPPRG")
		{
			return FILETYPE_YS2_PRG_B800;
		}
	}
	else if (ysNumber == 3)
	{
		if ((filename.length() >= 6) && (strncmp(&filename.c_str()[3], "MES", 3) == 0))
		{
			return FILETYPE_YS3_MES;
		}
	}
	return FILETYPE_NONE;
}

YsTextEncoding AlterMagic::getYsTextEncoding(YsFileType ysFileType)
{
	switch (ysFileType)
	{
		case FILETYPE_YS1_MESSAG:
		case FILETYPE_YS1_SHPMS:
		case FILETYPE_YS1_BMESAG:
			return CHARSET_YS1_FONT;
		case FILETYPE_YS2_SHPMS:
		case FILETYPE_YS2_EQPNAM:
		case FILETYPE_YS2_EVNMS:
		case FILETYPE_YS2_YSBKMS:
		case FILETYPE_YS2_MESCT:
			return CHARSET_YS2_SHIFTJIS;
		case FILETYPE_YS2_TTLPRG:
		case FILETYPE_YS2_ZDKPRG:
		case FILETYPE_YS2_MANPR1:
		case FILETYPE_YS2_ZDKPR:
			return CHARSET_YS2_SHIFTJIS_WIDEONLY;
		case FILETYPE_YS3_MES:
			return CHARSET_YS2_SHIFTJIS;
		case FILETYPE_NONE:
		default:
			return CHARSET_UNKNOWN;
	}
}

static uint32_t getPtrTblLen(const std::vector<uint8_t>& fileData, uint32_t fileSize, uint32_t tblOffset, uint32_t fileOffset)
{
	uint32_t i = tblOffset;
	uint32_t tblLen = 0;
	
	// Get table length based on first entry, assuming tight packing
	for (uint32_t j=i; j+1 < fileSize; j += 2)
	{
		uint32_t tblEntry = (fileData[j+0] + (fileData[j+1] << 8));
		
		if (tblEntry != 0)
		{
			tblLen = (tblEntry - tblOffset - fileOffset) / 2;
			break;
		}
	}
	
	return tblLen;
}

static uint32_t loadPtrTable(const std::vector<uint8_t>& fileData, uint32_t fileSize, uint32_t tblOffset, uint32_t fileOffset, std::vector<uint32_t>& offsetsOut)
{
	uint32_t i = tblOffset;
	uint32_t tblLen = getPtrTblLen(fileData, fileSize, tblOffset, fileOffset);
	
	offsetsOut.clear();
	for (int idx=0; idx < tblLen; idx++)
	{
		uint32_t tblEntry = (fileData[i+0] + (fileData[i+1] << 8));
		if (tblEntry != 0)
		{
			offsetsOut.push_back(tblEntry - fileOffset);
		}
		else
		{
			offsetsOut.push_back(0x0000);
		}
		i += 2;
	}
	
	return tblLen;
}

TextFile::TextFile() :
	mYsFileType(FILETYPE_NONE),
	mTextEncoding(CHARSET_YS1_FONT),
	mYsDisk(DISK_UNKNOWN),
	mMetaTableLen(0), mSubTableLen(), mTotalTableLen(0), mFileOffset(0), mStrings(),
	mBinPtrAddresses(), mBinFreeSpace()
{
}

TextFile::~TextFile()
{
}

bool TextFile::load(const DarmFS& fs, const std::string& filename, bool assumeEnFont)
{
	mBinPtrAddresses.clear();
	mBinFreeSpace.clear();
	
	// First detect disk
	mYsDisk = detectYsDisk(fs);
	int ysNumber = getYsNumber(mYsDisk);
	
	if (mYsDisk == DISK_UNKNOWN)
	{
		return false;
	}
	
	mYsFileType = getYsFileType(ysNumber, filename);
	mTextEncoding = getYsTextEncoding(mYsFileType);
	
	if (mYsFileType == FILETYPE_NONE)
	{
		return false;
	}
	
	const DarmFS::Entry* file = fs.getFile(filename.c_str());
	if (file == nullptr) return false;
	
	const std::vector<uint8_t>& fileData = file->mData;
	uint32_t fileSize = fileData.size();
	
	// The following two lines are technically heuristics... I don't think
	// the file format actually technically requires these to be valid.
	// This basically assumes no more than 128 strings in a message file,
	// that there is no padding, and that the files will be loaded into a
	// location that is a multiple of 256 bytes.
	if (mYsFileType == FILETYPE_YS1_MESSAG)
	{
		mFileOffset = 0x7000;
	}
	else if (mYsFileType == FILETYPE_YS1_SHPMS)
	{
		mFileOffset = 0x8000;
	}
	else if (mYsFileType == FILETYPE_YS1_BMESAG)
	{
		mFileOffset = 0x7000;
	}
	else if ((mYsFileType == FILETYPE_YS2_EVNMS) || (mYsFileType == FILETYPE_YS2_YSBKMS) || (mYsFileType == FILETYPE_YS2_MESCT))
	{
		mFileOffset = 0x6c00;
	}
	else if (mYsFileType == FILETYPE_YS2_EQPNAM)
	{
		mFileOffset = 0x6c00;
	}
	else if (mYsFileType == FILETYPE_YS2_SHPMS)
	{
		mFileOffset = 0x8000;
	}
	else if (mYsFileType == FILETYPE_YS2_TTLPRG)
	{
		mFileOffset = 0x0100;
	}
	else if (mYsFileType == FILETYPE_YS2_ZDKPRG)
	{
		mFileOffset = 0x4000;
	}
	else if (mYsFileType == FILETYPE_YS2_MANPR1)
	{
		mFileOffset = 0x0100;
	}
	else if (mYsFileType == FILETYPE_YS2_ZDKPR)
	{
		mFileOffset = 0x4400;
	}
	else if (mYsFileType == FILETYPE_YS3_MES)
	{
		mFileOffset = 0x6000;
	}
	
	std::vector<uint32_t> subTableOffsets;
	if (
		(mYsFileType == FILETYPE_YS1_MESSAG) ||
		(mYsFileType == FILETYPE_YS2_SHPMS) ||
		(mYsFileType == FILETYPE_YS3_MES)
		)
	{
		mMetaTableLen = loadPtrTable(fileData, fileSize, 0, mFileOffset, subTableOffsets);
		mSubTableLen.clear();
	}
	else
	{
		mMetaTableLen = 0;
		mSubTableLen.clear();
		subTableOffsets.push_back(0x0000);
	}
	
	// Iterate over tables to get length of subtables
	mTotalTableLen = 0;
	
	std::vector<std::vector<uint32_t>> ptrLists;
	if ((mYsFileType == FILETYPE_YS2_TTLPRG) || (mYsFileType == FILETYPE_YS2_ZDKPRG) || (mYsFileType == FILETYPE_YS2_MANPR1) || (mYsFileType == FILETYPE_YS2_ZDKPR) || (mYsFileType == FILETYPE_YS2_PRG_B800))
	{
		// Copy file data
		mBinData = fileData;
		mBinFreeSpace.clear();
		mBinFreeSpace.resize(mBinData.size(), 0);
	
		if (mYsFileType == FILETYPE_YS2_TTLPRG)
		{
			for (int i=0x4ce; i<0x510; i++)
			{
				mBinFreeSpace[i] = 1;
			}
			for (int i=0x12a7; i<0x1724; i++)
			{
				mBinFreeSpace[i] = 1;
			}
			for (int i=0x2eb1/*0x2d55*/; i<0x2f00; i++)
			{
				mBinFreeSpace[i] = 1;
			}
		}
		
		// YS2 TTLPRG String Finder
		uint32_t tableLen = 0;
	
		// Set up ptr list
		ptrLists.emplace_back();
		std::vector<uint32_t>& ptrList = ptrLists.back();
	
		size_t searchIdx = 0;
		while (searchIdx < (fileData.size()-9))
		{
			if ((mYsFileType == FILETYPE_YS2_TTLPRG) &&
				(fileData[searchIdx+0] == 0x21) &&
				(fileData[searchIdx+3] == 0x11) && (
					(
					(fileData[searchIdx+6] == 0xcd) &&
					(fileData[searchIdx+7] == 0x74) &&
					(fileData[searchIdx+8] == 0x08)
					) ||
					(
					(fileData[searchIdx+6] == 0xcd) &&
					(fileData[searchIdx+7] == 0x4a) &&
					(fileData[searchIdx+8] == 0x0b)
					) ||
					(
					(fileData[searchIdx+6] == 0xcd) &&
					(fileData[searchIdx+7] == 0x4e) &&
					(fileData[searchIdx+8] == 0x0b)
					))
				)
			{
				mBinPtrAddresses.push_back(searchIdx+1);
				
				uint32_t subTblEntry = fileData[searchIdx+1] + (fileData[searchIdx+2] << 8);
				ptrList.push_back(subTblEntry);
				tableLen++;
				
				printf("LOAD %x->%x\n", searchIdx+1, subTblEntry);
				
				searchIdx += 9;
				continue;
			}
			else if (((mYsFileType == FILETYPE_YS2_ZDKPRG) || (mYsFileType == FILETYPE_YS2_MANPR1) || (mYsFileType == FILETYPE_YS2_ZDKPR) || (mYsFileType == FILETYPE_YS2_PRG_B800)) &&
				(fileData[searchIdx+0] == 0xfd) &&
				(fileData[searchIdx+1] == 0x21) &&
					(
					(fileData[searchIdx+4] == 0xcd) &&
					(fileData[searchIdx+5] == 0xf0) &&
					(fileData[searchIdx+6] == 0x38)
					)
				)
			{
				mBinPtrAddresses.push_back(searchIdx+1);
				
				uint32_t subTblEntry = fileData[searchIdx+2] + (fileData[searchIdx+3] << 8);
				ptrList.push_back(subTblEntry);
				tableLen++;
				
				printf("LOAD %x->%x\n", searchIdx+1, subTblEntry);
				
				searchIdx += 7;
				continue;
			}
			searchIdx++;
		}
		
		mSubTableLen.push_back(tableLen);
		mTotalTableLen += tableLen;
	}
	else
	{
		// General case to figure out table layout
		for (uint32_t i=0; i<subTableOffsets.size(); i++)
		{
			uint32_t tblOffset = subTableOffsets[i];
			
			if ((tblOffset != 0) || (mMetaTableLen == 0))
			{
				uint32_t tableLen;
				if (mYsFileType == FILETYPE_YS1_BMESAG)
				{
					tableLen = 14;
				}
				else
				{
					tableLen = getPtrTblLen(fileData, fileSize, tblOffset, mFileOffset);
					
					// Find other tables starting that would terminate this
					for (uint32_t j=0; j<subTableOffsets.size(); j++)
					{
						if ((j != i) &&
							(subTableOffsets[j] > tblOffset))
						{
							// If the next subtable starts early...
							uint32_t tableLenFromNext = (subTableOffsets[j] - tblOffset) / 2;
							if (tableLenFromNext < tableLen)
							{
								tableLen = tableLenFromNext;
							}
						}
						
						// If it's earlier in the subtables, also compare vs messages
						if (j < i)
						{
							for (uint32_t k=0; k<mSubTableLen[j]; k++)
							{
								uint32_t entryOffset = subTableOffsets[j]+k*2;
								uint32_t subTblEntry = (fileData[entryOffset+0] + (fileData[entryOffset+1] << 8));
								if (subTblEntry >= mFileOffset)
								{
									subTblEntry -= mFileOffset; // Convert to offset in file
									if (subTblEntry > tblOffset)
									{
										uint32_t tableLenFromNext = (subTblEntry - tblOffset) / 2;
										if (tableLenFromNext < tableLen)
										{
											tableLen = tableLenFromNext;
										}
									}
								}
							}
						}
					}
				}
				mSubTableLen.push_back(tableLen);
				mTotalTableLen += tableLen;
			}
			else
			{
				mSubTableLen.push_back(0);
			}
		}
		
		for (uint32_t section=0; section<mSubTableLen.size(); section++)
		{
			// Set up ptr list
			ptrLists.emplace_back();
			std::vector<uint32_t>& ptrList = ptrLists.back();
			
			uint32_t tableOffset = subTableOffsets[section];
			uint32_t tableLen = mSubTableLen[section];
			for (uint32_t idx=0;idx<tableLen;idx++)
			{
				uint32_t strEntryOffset = tableOffset + idx*2;
				uint32_t strOffset = fileData[strEntryOffset+0] + (fileData[strEntryOffset+1] << 8);
				ptrList.push_back(strOffset);
			}
		}
		// End of general case for table layout
	}

	// Get the data bytes for each string
	std::vector<std::vector<uint8_t>> stringList;
	for (uint32_t section=0; section<mSubTableLen.size(); section++)
	{
		uint32_t tableOffset = subTableOffsets[section];
		uint32_t tableLen = mSubTableLen[section];
		for (uint32_t idx=0;idx<tableLen;idx++)
		{
			stringList.emplace_back();
			std::vector<uint8_t>& s = stringList.back();
			
			// TODO: assertions?
			uint32_t strOffset = ptrLists[section][idx];
			if ((strOffset == 0x0000)|| (strOffset < (mFileOffset + tableOffset)) || (strOffset > (mFileOffset + fileSize)))
			{
				continue;
			}
			strOffset -= mFileOffset;
			
			uint32_t readLimit = fileSize;
			if ((section+1)<mSubTableLen.size())
			{
				uint32_t nextTableOffset = subTableOffsets[section+1];
				if (nextTableOffset < readLimit)
				{
					readLimit = nextTableOffset;
				}
			}
			
			// Copy sting data
			s.clear();
			for (uint32_t j=strOffset; /*(j < readLimit) &&*/ (fileData[j] != 0xff) && !((mYsFileType == FILETYPE_YS3_MES) && ((fileData[j] == 0x00) || (fileData[j] == 0x01))); j++)
			{
				s.push_back(fileData[j]);
				
				if ((fileData[j] == '\r') && (
						((mYsFileType == FILETYPE_YS1_MESSAG) && (section == 3)) ||
						(mYsFileType == FILETYPE_YS2_EQPNAM)
					))
				
				{
					// SPECIAL CASE: MESSAG 4th section terminate after newline
					break;
				}
			}
			for (uint32_t j=strOffset; j<=(strOffset+s.size()); j++)
			{
				// Mark as free space
				if (j < mBinFreeSpace.size())
				{
					mBinFreeSpace[j] = 1;
				}
			}
		}
	}
	
	if ((mYsFileType == FILETYPE_YS2_TTLPRG) || (mYsFileType == FILETYPE_YS2_ZDKPRG) || (mYsFileType == FILETYPE_YS2_MANPR1) || (mYsFileType == FILETYPE_YS2_ZDKPR) || (mYsFileType == FILETYPE_YS2_PRG_B800))
	{
		// DBG
		uint32_t ii;
		for (ii=0; (ii < fileData.size()) && (ii < mBinFreeSpace.size()); ii++)
		{
			if (mBinFreeSpace[ii] == 0) continue;
			uint32_t endIdx=ii+1;
			for (; endIdx < fileData.size(); endIdx++)
			{
				if (mBinFreeSpace[endIdx] != 0) continue;
				break;
			}
			printf("FREE: %04x-%04x\n", ii, endIdx);
			ii = endIdx;
		}
		fflush(stdout);
	}
	
	// Convert strings to unicode
	mStrings.clear();
	for (auto& s: stringList)
	{
		// Convert to wstring with mapping to unicode
		mStrings.push_back(decodeString(mTextEncoding, s, assumeEnFont));
	}
	
	return true;
}

std::vector<uint8_t> TextFile::toEncodedBytesPrg() const
{
	std::vector<uint8_t> s = mBinData;
	std::vector<uint8_t> freeSpace = mBinFreeSpace;
	std::unordered_map<std::wstring, uint32_t> existingMessages;

	uint32_t flatIdx = 0;
	for (uint32_t section=0; section<mSubTableLen.size(); section++)
	{
		// Only reference within sections
		existingMessages.clear();
		
		uint32_t tableLen = mSubTableLen[section];
		if (tableLen == 0)
		{
			continue;
		}
		
		for (uint32_t idx=0;idx<tableLen;idx++)
		{
			uint32_t ptrAddr = mBinPtrAddresses[flatIdx];
			
			std::wstring str = mStrings[flatIdx];
			auto existingIt = existingMessages.find(str);
			if (existingIt != existingMessages.end())
			{
				// Found match, use it
				uint32_t foundOffset = existingIt->second;
				s[ptrAddr] = (foundOffset >> 0) & 0xFF;
				s[ptrAddr+1] = (foundOffset >> 8) & 0xFF;
				flatIdx++;
				continue;
			}

			std::vector<uint8_t> strData;
			encodeString(mTextEncoding, strData, str);
			strData.push_back(0xFF);
			uint32_t encLen = strData.size();
			
			// Find space for string
			uint32_t freeIdx;
			for (freeIdx=0; freeIdx < s.size(); freeIdx++)
			{
				if (freeSpace[freeIdx] == 0) continue;
				uint32_t endIdx=freeIdx+1;
				for (; endIdx < s.size(); endIdx++)
				{
					if (freeSpace[endIdx] != 0) continue;
					break;
				}
				uint32_t availSpace = (endIdx - freeIdx);
				if (availSpace >= encLen)
				{
					for (uint32_t i=0; i<encLen; i++)
					{
						s[freeIdx+i] = strData[i];
						freeSpace[freeIdx+i] = 0;
					}
					break;
				}
			}
			if (freeIdx >= s.size())
			{
				s.resize(freeIdx + encLen, 0);
				freeSpace.resize(s.size(), 0);
				for (uint32_t i=0; i<encLen; i++)
				{
					s[freeIdx+i] = strData[i];
					freeSpace[freeIdx+i] = 0;
				}
				printf("EXTEND");
			}
			
			printf("SAVE %x->%x\n", ptrAddr, mFileOffset + freeIdx);
			
			// Set address
			uint32_t strMemOffset = mFileOffset + freeIdx;
			s[ptrAddr] = (strMemOffset >> 0) & 0xFF;
			s[ptrAddr+1] = (strMemOffset >> 8) & 0xFF;
			existingMessages[str] = strMemOffset;
			
			// Add the string

			flatIdx++;
		}
	}
	fflush(stdout);
	
	return s;
}

void TextFile::writeStringWithPtr(
	std::vector<uint8_t>& s,
	std::unordered_map<std::wstring,uint32_t>& existingMessages,
	uint32_t tblOffset,
	uint32_t idx,
	std::wstring str) const
{
	auto existingIt = existingMessages.find(str);
	if (existingIt != existingMessages.end())
	{
		// Found match, use it
		uint32_t foundOffset = existingIt->second;
		s[tblOffset + idx*2 + 0] = (foundOffset >> 0) & 0xFF;
		s[tblOffset + idx*2 + 1] = (foundOffset >> 8) & 0xFF;
		return;
	}
	
	// Set address
	uint32_t strMemOffset = mFileOffset + s.size();
	s[tblOffset + idx*2 + 0] = (strMemOffset >> 0) & 0xFF;
	s[tblOffset + idx*2 + 1] = (strMemOffset >> 8) & 0xFF;
	existingMessages[str] = strMemOffset;
	
	// Add the string
	encodeString(mTextEncoding, s, str);
	if (mYsFileType == FILETYPE_YS3_MES)
	{
		s.push_back(0x00);
	}
	else
	{
		s.push_back(0xFF);
	}
}

std::vector<uint8_t> TextFile::toEncodedBytes() const
{
	if ((mYsFileType == FILETYPE_YS2_TTLPRG) || (mYsFileType == FILETYPE_YS2_ZDKPRG) || (mYsFileType == FILETYPE_YS2_MANPR1) || (mYsFileType == FILETYPE_YS2_ZDKPR) || (mYsFileType == FILETYPE_YS2_PRG_B800))
	{
		return toEncodedBytesPrg();
	}
	
	std::vector<uint8_t> s;
	std::unordered_map<std::wstring, uint32_t> existingMessages;
	
	// Add space for meta table...
	for (int i=0; i < mMetaTableLen; i++)
	{
		s.push_back(0x00);
		s.push_back(0x00);
	}

	// All sub-stables at start?
	bool bAllSubtablesAtStart = (mYsFileType == FILETYPE_YS3_MES);

	uint32_t flatIdx = 0;
	std::vector<uint32_t> tblOffsetsBySection;
	for (uint32_t section=0; section<mSubTableLen.size(); section++)
	{
		uint32_t tableLen = mSubTableLen[section];
		if (tableLen == 0)
		{
			continue;
		}
		
		uint32_t tblOffset = s.size();
		if (section < mMetaTableLen)
		{
			uint32_t tblMemOffset = mFileOffset + tblOffset;
			s[section*2 + 0] = (tblMemOffset >> 0) & 0xFF;
			s[section*2 + 1] = (tblMemOffset >> 8) & 0xFF;
		}
		
		// Add space for sub-table
		for (int i=0; i < tableLen; i++)
		{
			s.push_back(0xFF);
			s.push_back(0xFF);
		}
		
		if (bAllSubtablesAtStart)
		{
			// All subtables at start?
			tblOffsetsBySection.push_back(tblOffset);
		}
		else
		{
			// Write messages after subtable
			
			existingMessages.clear();
			for (uint32_t idx=0;idx<tableLen;idx++)
			{
				std::wstring str = mStrings[flatIdx];
				
				writeStringWithPtr(
					s, existingMessages,
					tblOffset, idx,
					mStrings[flatIdx]);
				flatIdx++;
			}
		}
	}
	
	if (bAllSubtablesAtStart)
	{
		// If all text is after all subtables, write messages now
		for (uint32_t section=0; section<mSubTableLen.size(); section++)
		{
			uint32_t tableLen = mSubTableLen[section];
			if (tableLen == 0)
			{
				continue;
			}
			uint32_t tblOffset = tblOffsetsBySection[section];
			
			// Write all messages for section
			existingMessages.clear();
			for (uint32_t idx=0;idx<tableLen;idx++)
			{
				std::wstring str = mStrings[flatIdx];
				
				if ((str == L"") && ((idx+1) == tableLen))
				{
					flatIdx;
					continue;
				}
				
				writeStringWithPtr(
					s, existingMessages,
					tblOffset, idx,
					mStrings[flatIdx]);
				flatIdx++;
			}
		}
	}
	
	return s;
}

std::wstring TextFile::exportTextToWString() const
{
	std::wstringstream sBuilder;
	if (mSubTableLen.size() > 1)
	{
		int totalIdx = 0;
		for (uint32_t section=0; section<mSubTableLen.size(); section++)
		{
			uint32_t tableLen = mSubTableLen[section];
			for (uint32_t idx=0;idx<tableLen;idx++)
			{
				sBuilder << "==== ";
				sBuilder << std::to_wstring(section+1) << L"," << std::to_wstring(idx+1);
				sBuilder << " ====\r\n";
				sBuilder << mStrings[totalIdx];
				sBuilder << "\r\n";
				totalIdx++;
			}
		}
	}
	else
	{
		for (uint32_t idx=0;idx<mTotalTableLen;idx++)
		{
			sBuilder << "==== ";
			sBuilder << std::to_wstring(idx+1);
			sBuilder << " ====\r\n";
			sBuilder << mStrings[idx];
			sBuilder << "\r\n";
		}
	}
	return sBuilder.str();
}

bool TextFile::importTextFromWString(std::wstring str)
{
	std::vector<std::wstring> stringsFound;
	while (str.size() > 0)
	{
		// Check if we need to skip to the first ==== divider
		if (str.rfind(L"====", 0) != 0)
		{
			// Not starting with ==== so find first ==== line
			std::wstring::size_type pos = str.find(L"\r\n====");
			if (pos == std::wstring::npos)
			{
				// Not found, abort
				break;
			}
			str = str.substr(pos+2);
		}
		
		// Skip past ==== line
		{
			std::wstring::size_type pos = str.find(L"\r\n");
			if (pos == std::wstring::npos)
			{
				// Not found, abort
				break;
			}
			str = str.substr(pos+2);
		}
		
		// Find end of string
		{
			std::wstring::size_type pos = str.find(L"\r\n====");
			if (pos == std::wstring::npos)
			{
				pos = str.size();
				if (str.rfind(L"\r\n") == pos-2)
				{
					pos -= 2;
				}
				stringsFound.push_back(str.substr(0, pos));
				break;
			}
			stringsFound.push_back(str.substr(0, pos));
			str = str.substr(pos+2);
		}
	}
	
	if (mStrings.size() != stringsFound.size())
	{
		// Length mismatch
		return false;
	}
	
	// Replace data
	mStrings = stringsFound;
	return true;
}

std::string TextFile::exportTextToUTF8() const
{
	std::wstring_convert<std::codecvt_utf8<wchar_t>> conv;
	return "\xef\xbb\xbf" + conv.to_bytes(exportTextToWString());
}

bool TextFile::importTextFromUTF8(std::string str)
{
	if (str.rfind("\xef\xbb\xbf", 0) == 0)
	{
		// Trim BOM if present
		str = str.substr(3);
	}
	std::wstring_convert<std::codecvt_utf8<wchar_t>> conv;
	return importTextFromWString(conv.from_bytes(str));
}

/*static*/ std::vector<std::string> TextFile::exportAllText(const DarmFS& fs, const std::filesystem::path& path, bool assumeEnFont)
{
	std::vector<std::string> fileList;
	
	for (auto it = fs.cbegin(); it != fs.cend(); it++)
	{
		const DarmFS::Entry& entry = *it;
		
		// Load as text file if possible
		TextFile textFile;
		if (!textFile.load(fs, entry.mName, assumeEnFont))
		{
			// Not a usable text file
			continue;
		}
		
		// Get path
		std::string fileName = std::string(entry.mName) + ".txt";
		std::filesystem::path filePath = path;
		filePath /= fileName;
		
		// Export to file
		std::string fileData = textFile.exportTextToUTF8();
		writeFile(filePath, (const uint8_t*)fileData.c_str(), fileData.length());
		
		// Add to list
		fileList.push_back(fileName);
	}
	
	return fileList;
}

/*static*/ std::vector<std::string> TextFile::importAllText(DarmFS& fs, const std::filesystem::path& path)
{
	std::vector<std::string> fileList;
	std::vector<std::string> diskFiles;
	for (auto it = fs.cbegin(); it != fs.cend(); it++)
	{
		const DarmFS::Entry& entry = *it;
		diskFiles.push_back(entry.mName);
	}
	
	for (const std::string& fn : diskFiles)
	{
		// Load as text file if possible
		TextFile textFile;
		if (!textFile.load(fs, fn, true))
		{
			// Not a usable text file
			continue;
		}
		
		// Get path
		std::string fileName = fn + ".txt";
		std::filesystem::path filePath = path;
		filePath /= fileName;
		
		// Try to read file
		std::vector<uint8_t> txtFileData;
		if (!readFile(filePath, txtFileData))
		{
			// Can't read file
			continue;
		}
		std::string txtFileDataStr((const char*)&txtFileData[0], txtFileData.size());
		
		// Export to file
		if (!textFile.importTextFromUTF8(txtFileDataStr))
		{
			#if defined(_DARMKEY_CMDONLY_)
				std::cout << fileName << " is malformed. Incorrect number of entries.";
			#else
				nana::msgbox mb("Error");
				mb << fileName << " is malformed. Incorrect number of entries.";
				mb.show();
			#endif
			continue;
		}
		
		// Encode text
		std::vector<uint8_t> fileData = textFile.toEncodedBytes();
		
		// Pad to even size
		fileData.resize(fileData.size() + (fileData.size() % 2), 0xFF); 
		
		// Check if the file has changed
		const DarmFS::Entry* entry = fs.getFile(fn.c_str());
		if ((entry == nullptr) || (fileData == entry->mData))
		{
			// File unchanged
			continue;
		}
		
		// Update data
		fs.writeFile(fn.c_str(), &fileData[0], fileData.size());
		
		// Add to list
		fileList.push_back(fn);
	}
	
	return fileList;
}
