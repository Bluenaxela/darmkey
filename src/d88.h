#ifndef _D88_INCLUDE
#define _D88_INCLUDE

#include <cstdint>
#include <vector>
#include <string>

class D88Image
{
private:
	typedef std::vector<uint8_t> sector_t;
	typedef std::vector<sector_t> sectorlist_t;
	typedef std::vector<sectorlist_t> sidelist_t;
	typedef std::vector<sidelist_t> tracklist_t;
	struct disk_t {
		std::string title;
		tracklist_t tracks;
	};
	std::vector<disk_t> mDisks;
	bool mValid;
	
public:
	D88Image();
	~D88Image();
	
	void loadData(const uint8_t* data, uint32_t size);
	void saveToBytes(std::vector<uint8_t>& out);

	const uint8_t* getSector(uint32_t disk, uint8_t track, uint8_t side, uint8_t sector, uint16_t& sizeOut);
	
	void copyRange(uint32_t disk, uint8_t track, uint8_t side, uint8_t sector, uint8_t endTrack, uint8_t endSide, uint8_t endSector, std::vector<uint8_t>& out);
	bool writeData(uint32_t disk, uint8_t& track, uint8_t& side, uint8_t& sector, uint8_t& endTrack, uint8_t& endSide, uint8_t& endSector, const uint8_t* data, uint32_t size, uint8_t pad = 0x00);
	
	bool isValid() { return mValid; }
	uint32_t getDiskCount() { return mDisks.size(); }
	std::string getDiskTitle(uint32_t disk) { return (disk < mDisks.size()) ? mDisks[disk].title : "<INVALID>"; }
};


#endif
