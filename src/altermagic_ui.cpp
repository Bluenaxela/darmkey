#include <nana/gui/widgets/listbox.hpp>
#include <nana/system/dataexch.hpp>
#include <string>
#include <vector>
#include <sstream>
#include <ios>
#include <unordered_map>
#include <cstring>
#include "altermagic.h"
#include "altermagic_ui.h"
#include "darmfs.h"
#include "d88.h"
#include "fs_compat.h"

#include "charset_ys2_shiftjis.h"
#include "charset_ys1.h"

#include <iostream>

//=============================================================
using namespace AlterMagic;

AlterMagicUI* gRef = nullptr; // This is a little naughty, but oh well

template<bool bEnabled>
class inline_widget : public nana::listbox::inline_notifier_interface
{
private:
	//Creates inline widget
	//listbox calls this method to create the widget
	//The position and size of widget can be ignored in this process
	virtual void create(nana::window wd) override
	{
		mIgnoringChange = false;
		
		//Create listbox
		txt_.create(wd);
		
		nana::paint::font ft(10.0, "migu-1m-regular.ttf");
		txt_.typeface(ft);
		txt_.editable(bEnabled);
		
		/*
		txt_.events().click([this]
		{
			//Select the item when clicks the textbox
			indicator_->selected(pos_);
		});
		*/

		txt_.events().mouse_move([this]
		{
			//Highlight the item when hovers the textbox
			indicator_->hovered(pos_);
		});
		
		txt_.events().mouse_wheel([this](const nana::arg_wheel& arg)
		{
			if (arg.which == nana::arg_wheel::wheel::vertical)
			{
				if (arg.upwards)
				{
					// Up
				}
				else
				{
					// Down
				}
			}
		});
		
		txt_.events().text_changed([this]()
		{
			if (!mIgnoringChange)
			{
				indicator_->modify(pos_, txt_.caption());
				bool bNewUnsaved = true;
				if ((gRef != nullptr) && bEnabled && (gRef->mUnsaved != bNewUnsaved))
				{
					gRef->mUnsaved = bNewUnsaved;
					gRef->setTitle();
				}
			}
		});
	}

	virtual void activate(inline_indicator& ind, index_type pos)
	{
		indicator_ = &ind;
		pos_ = pos;
	}

	void notify_status(status_type status, bool status_on) override
	{
		//Sets focus for the textbox when the item is selected
		if((status_type::selecting == status) && status_on)
			txt_.focus();
	}

	//Sets the inline widget size
	//dimension represents the max size can be set
	//The coordinate of inline widget is a logical coordinate to the sub item of listbox
	void resize(const nana::size& dimension) override
	{
		auto sz = dimension;
		//sz.width -= (sz.width < 50 ? 0 : 50); //Check for avoiding underflow.
		txt_.size(sz);
	}

	//Sets the value of inline widget with the value of the sub item
	virtual void set(const value_type& value)
	{
		//Avoid emitting text_changed to set caption again, otherwise it
		//causes infinite recursion.
		if(txt_.caption() != value)
		{
			mIgnoringChange = true;
			txt_.caption(value);
			mIgnoringChange = false;
		}
	}

	//Determines whether to draw the value of sub item
	//e.g, when the inline widgets covers the whole background of the sub item,
	//it should return false to avoid listbox useless drawing
	bool whether_to_draw() const override
	{
		return false;
	}
private:
    inline_indicator * indicator_{ nullptr };
    index_type pos_;
    nana::textbox txt_;
	bool mIgnoringChange;
};





//=============================================================


AlterMagicUI::AlterMagicUI(const nana::form& fm) :
	mForm(fm, nana::size{500, 400}), mReferenceImg(), mShowing(false), mCurrentFilename(), mCurrentDisk(0), mUnsaved(false),
	mList(mForm, nana::rectangle{ 10, 10, 300, 200 }), mSaveButton(mForm), mReferenceLabel(mForm), mReferenceBrowse(mForm), mMetaLabel(mForm), mOpenFBox(mForm, true), mModEncCheckbox(mForm),
	copyTextButton(mForm), copyRefButton(mForm)
{
	mForm.caption("Alter Magic");
	mForm.events().unload([this](const nana::arg_unload& arg){
		arg.cancel = true;
		mForm.hide();
	});
	
	mList.sortable(false);
	mList.column_movable(false);
	mList.column_resizable(false);
	mList.scheme().item_height_ex = 200;
	mList.scheme().min_column_width = 200;
	mList.scheme().mouse_wheel.lines = 1;
	mList.append_header("#");
	mList.append_header("Text");
	mList.append_header("Ref");
	
	mList.at(0).inline_factory(1, nana::pat::make_factory<inline_widget<true>>());
	mList.at(0).inline_factory(2, nana::pat::make_factory<inline_widget<false>>());
	
	mList.column_at(0).width(32);
	mList.column_at(1).width(200);
	mList.column_at(2).width(200);
	
	mList.events().resized([this](const nana::arg_resized& arg)
	{
		mList.column_at(1).width((arg.width-64)/2);
		mList.column_at(2).width((arg.width-64)/2);
	});
	
	
	mSaveButton.caption("Save to Darm Key");
	mReferenceLabel.caption("Reference File: <None>");
	mReferenceLabel.text_align(nana::align::center, nana::align_v::center);
	mReferenceBrowse.caption("Load Reference D88");
	
	mMetaLabel.caption("");
	mMetaLabel.text_align(nana::align::center, nana::align_v::center);

	mOpenFBox.allow_multi_select(false);
	mOpenFBox.add_filter("D88 File", "*.d88");
	mOpenFBox.add_filter("All Files", "*.*");
	mReferenceBrowse.events().click([this]{
		auto files = mOpenFBox.show();
		if (!files.empty())
		{
			std::vector<uint8_t> fileData;
			if (!readFile(files[0], fileData))
			{
				nana::msgbox mb("Error");
				mb << "Couldn't read file";
				mb.show();
				return;
			}
			
			// Parse disk iamge and filesystem
			mReferenceImg.loadData(&fileData[0], fileData.size());
			std::stringstream refString;
			refString << "Reference File: " << files[0].filename();
			mReferenceLabel.caption(refString.str());
			
			tryLoadReference();
		}
	});
	
	mSaveButton.events().click([this]{
		saveToDarmKey();
	});
	
	mModEncCheckbox.caption("Assume EN-Font (Text side only, doesn't affect Ref)");
	mModEncCheckbox.check(true);
	
	mModEncCheckbox.events().checked([this]{
		bool assumeEnFont = mModEncCheckbox.checked();
		
		mList.auto_draw(false);
		for (int idx=0; idx<mTextFile.getTotalTableLength(); idx++)
		{
			std::vector<uint8_t> s;
			std::wstring str = nana::to_wstring(mList.at(0).at(idx).text(1));
			encodeString(mTextFile.getTextEncoding(), s, str);
			str = decodeString(mTextFile.getTextEncoding(), s, assumeEnFont);
			mList.at(0).at(idx).text(1, str);
		}
		mList.auto_draw(true);
	});
	
	copyTextButton.caption("Copy All Text Rows");
	copyRefButton.caption("Copy All Ref Rows");
	
	copyTextButton.events().click([this]{
		copyRowsToClipboard(1);
	});
	copyRefButton.events().click([this]{
		copyRowsToClipboard(2);
	});
	
    //Layout management
    mForm.div("vert <weight=24 topBar><weight=32 metadata><weight=16 radios><weight=24 copyButtons><panel>");
	mForm["topBar"] << mSaveButton << mReferenceLabel << mReferenceBrowse;
	mForm["radios"] << mModEncCheckbox;
	mForm["metadata"] << mMetaLabel;
	mForm["copyButtons"] << copyTextButton << copyRefButton;
	mForm["panel"] << mList;
	
    mForm.collocate();
	
	gRef = this;
}

AlterMagicUI::~AlterMagicUI()
{
	
}

void AlterMagicUI::show(DarmFS& fs, uint32_t disk, const std::string& filename)
{
	// Clear state
	mCurrentFilename = filename;
	mCurrentDisk = disk;
	mList.clear();
	mUnsaved = false;	
	
	// Try to load file
	if (!mTextFile.load(fs, filename, mModEncCheckbox.checked()))
	{
		// Failed to load, check why.
		nana::msgbox mb("Error");
		if (mTextFile.getDiskType() == DISK_UNKNOWN)
		{
			mb << "The current disk is unrecognized.";
		}
		else if (mTextFile.getFileType() == FILETYPE_NONE)
		{
			mb << "Format of " << filename << " is unknown.";
		}
		else
		{
			mb << "Unknown error opening " << filename << ".";
		}
		mb.show();
		return;
	}

	// Set metadata label
	{
		std::stringstream metaString;
		metaString << mTextFile.getTotalTableLength() << " message slots found.";
		if (mTextFile.getSubTableCount() > 1)
		{
			metaString << "\nDivided into " << mTextFile.getSubTableCount() << " tables.";
		}
		metaString << " Loads to: 0x" << std::hex << mTextFile.getFileOffset();
		mMetaLabel.caption(metaString.str());
	}
	
	// Add empty rows
	mList.auto_draw(false);
	mList.clear();
	if (mTextFile.getSubTableCount() > 1)
	{
		for (uint32_t section=0; section<mTextFile.getSubTableCount(); section++)
		{
			uint32_t tableLen = mTextFile.getSubTableLengths()[section];
			for (uint32_t idx=0;idx<tableLen;idx++)
			{
				mList.at(0).append({std::to_wstring(section+1)+L","+std::to_wstring(idx+1), L"", L""});
			}
		}
	}
	else
	{
		for (uint32_t idx=0;idx<mTextFile.getTotalTableLength();idx++)
		{
			mList.at(0).append({std::to_wstring(idx+1), L"", L""});
		}
	}
	mList.auto_draw(true);
		
	// Set main row data
	setColumnForData(mTextFile.getStrings(), 1);
	
	// Set reference data
	tryLoadReference();
	
	// Set window title
	setTitle();
	
	// Handle layout
	mForm.collocate();
	mForm.show();
	mShowing = true;
}

void AlterMagicUI::setColumnForData(const std::vector<std::wstring>& strings, uint32_t column)
{
	// Reset widgets
	mList.auto_draw(false);
	
	int idx = 0;
	for (const auto& str: strings)
	{
		mList.at(0).at(idx).text(column, str);
		idx++;
	}
	mList.auto_draw(true);
}

void AlterMagicUI::tryLoadReference()
{
	DarmFS fs;
	fs.load(mReferenceImg, mCurrentDisk);
	std::string filename = mCurrentFilename;

	// Special case for refencing MESS12
	if (filename == "MESS12")
	{
		filename = "MESSAG";
	}
	
	TextFile textFile;
	if (!textFile.load(fs, filename, false))
	{
		// Clear column
		mList.auto_draw(false);
		for (int idx=0; idx<textFile.getTotalTableLength(); idx++)
		{
			mList.at(0).at(idx).text(2, L"");
		}
		mList.auto_draw(true);
		return;
	}

	// Try to set reference column
	setColumnForData(textFile.getStrings(), 2);
}

void AlterMagicUI::setTitle()
{
	std::stringstream titleString;
	titleString << "Alter Magic - " << mCurrentFilename;
	if (mUnsaved)
	{
		titleString << " - UNSAVED";
	}
	mForm.caption(titleString.str());
}

void AlterMagicUI::hide()
{
	mShowing = false;
	mForm.hide();
}

extern bool updateFilesystemFile(uint32_t disk, const char* filename, const std::vector<uint8_t>& filedata);

void AlterMagicUI::saveToDarmKey()
{
	// Get text from UI
	for (int idx=0; idx < mTextFile.getTotalTableLength(); idx++)
	{
		std::wstring str = nana::to_wstring(mList.at(0).at(idx).text(1));
		mTextFile.setString(idx, str);
	}
	
	// Encode file
	std::vector<uint8_t> s = mTextFile.toEncodedBytes();
	
	// Save to filesystem via DarmKey
	if (updateFilesystemFile(mCurrentDisk, mCurrentFilename.c_str(), s))
	{
		mUnsaved = false;
		setTitle();
	}
}

void AlterMagicUI::copyRowsToClipboard(uint32_t col)
{
	bool assumeEnFont = mModEncCheckbox.checked();

	std::wstringstream sBuilder;
	for (int idx=0; idx < mTextFile.getTotalTableLength(); idx++)
	{
		sBuilder << "==== " << nana::to_wstring(mList.at(0).at(idx).text(0)) << " ====\n";
		sBuilder << nana::to_wstring(mList.at(0).at(idx).text(col)) << "\n";
	}
	nana::system::dataexch().set(sBuilder.str(), nana::API::root(mForm));
}

//=============================================================================
//=============================================================================
//=============================================================================
