#ifndef _FS_COMPAT_H_
#define _FS_COMPAT_H_

#include <filesystem>
#include <vector>
#include <cstdint>

bool readFile(const std::filesystem::path& path, std::vector<uint8_t>& out);
bool writeFile(const std::filesystem::path& path, const uint8_t* data, uint32_t size);

#endif