#include "darmfs.h"
#include "d88.h"
#include <cstring>

DarmFS::DarmFS() :
	mInitTrack(0),
	mInitSide(0),
	mInitSector(0),
	mEntries(), mEdited(false)
{
}

DarmFS::~DarmFS()
{
}

void DarmFS::load(D88Image& img, uint32_t disk)
{
	mEntries.clear();
	mEdited = false;
	if (disk >= img.getDiskCount()) return;
	
	bool bHaveFirstSector = false;
	
	// Read FAT table sectors
	std::vector<uint8_t> fatTable;
	img.copyRange(disk, 0, 1, 1, 0, 1, 2, fatTable);
	
	for (int i=0; i < fatTable.size(); i += 16)
	{
		if (fatTable[i] == 0xFF) continue;
		
		mEntries.emplace_back();
		Entry& entry = mEntries.back();
		
		// Copy filename
		memcpy(entry.mName, &fatTable[i], 6);
		entry.mName[6] = '\0';
		entry.mEdited = false;
		
		int startTrack  = fatTable[i+12] >> 1;
		int startSide   = fatTable[i+12] & 1;
		int startSector = fatTable[i+13];
		int endTrack    = fatTable[i+14] >> 1;
		int endSide     = fatTable[i+14] & 1;
		int endSector   = fatTable[i+15];
		
		if ((!bHaveFirstSector) ||
			(startTrack < mInitTrack) ||
			((startTrack == mInitTrack) && (startSide < mInitSide)) ||
			((startTrack == mInitTrack) && (startSide == mInitSide) && (startSector < mInitSector))
			)
		{
			bHaveFirstSector = true;
			mInitTrack  = startTrack;
			mInitSide   = startSide;
			mInitSector = startSector;
		}
		
		std::vector<uint8_t> fileData;
		img.copyRange(
			disk,
			startTrack, startSide, startSector,
			endTrack,   endSide,   endSector,
			entry.mData);
		
		// Figure out the exact file size
		uint16_t fileSize = (256 * fatTable[i+11]) + fatTable[i+10];
		fileSize -= (256 * fatTable[i+9]) + fatTable[i+8];
		if ((fileSize % 2) == 1)
		{
			fileSize++;
		}
		
		// Resize file data vector appropriately
		if (fileSize <= entry.mData.size())
		{
			entry.mData.resize(fileSize);
		}
	}
}

void DarmFS::save(D88Image& img, uint32_t disk)
{
	if (disk >= img.getDiskCount()) return;
	std::vector<uint8_t> fatTable;
	
	// Start first file where it starts in the source disk we loaded
	uint8_t track = mInitTrack;
	uint8_t side = mInitSide;
	uint8_t sector = mInitSector;
	
	for (Entry& entry : mEntries)
	{
		// Create FAT entry
		uint32_t i = fatTable.size();
		fatTable.resize(i + 16);
		
		// Copy filename
		memcpy(&fatTable[i], entry.mName, 6);
		
		// Get file size rounded down to next odd number to save, for reasons
		uint32_t fileSize = entry.mData.size();
		fileSize -= (1 - (fileSize % 2));
		
		// Set metadata
		fatTable[i+6]  = 0;    // Unknown
		fatTable[i+7]  = 0x4d; // Unknown
		fatTable[i+8]  = 0;    // Negative Filesize LSBs?!?
		fatTable[i+9]  = 0;    // Negative Filesize MSBs?!?
		fatTable[i+10] = fileSize & 0xFF; // Filesize LSB
		fatTable[i+11] = fileSize >> 8;   // Filesize MSB
		fatTable[i+12] = track * 2 + side;
		fatTable[i+13] = sector;
		
		// Write data (this will increment track/side/sector and set endTrack/endSide/endSector)
		uint8_t endTrack, endSide, endSector;
		img.writeData(disk, track, side, sector, endTrack, endSide, endSector, &entry.mData[0], entry.mData.size(), 0);
		
		fatTable[i+14] = endTrack * 2 + endSide;
		fatTable[i+15] = endSector;
		
		entry.mEdited = false;
	}
	
	// Write the FAT table itself
	{
		track = 0;
		side = 1;
		sector = 1;
		uint8_t endTrack, endSide, endSector;
		img.writeData(disk, track, side, sector, endTrack, endSide, endSector, &fatTable[0], fatTable.size(), 0xFF);
	}
	
	mEdited = false;
}

DarmFS::Entry* DarmFS::getFileInternal(const char* name)
{
	if (name == nullptr) return nullptr;
	for (Entry& entry : mEntries)
	{
		if (strcmp(name, entry.mName) == 0)
		{
			return &entry;
		}
	}
	return nullptr;
}

const DarmFS::Entry* DarmFS::getFileInternalConst(const char* name) const
{
	if (name == nullptr) return nullptr;
	for (const Entry& entry : mEntries)
	{
		if (strcmp(name, entry.mName) == 0)
		{
			return &entry;
		}
	}
	return nullptr;
}

bool DarmFS::deleteFile(const char* name)
{
	if (name == nullptr) return false;
	for(auto it = mEntries.begin(); it != mEntries.end(); it++)
	{
		if (strcmp(name, (*it).mName) == 0)
		{
			mEntries.erase(it);
			mEdited = true;
			return true;
		}
	}
	return false;
}

DarmFS::Entry* DarmFS::writeFile(const char* name, const uint8_t* data, size_type size)
{
	if (name == nullptr) return nullptr;
	
	// Get file entry if existing
	Entry* entry = getFileInternal(name);
	
	// Make new file if necessary
	if (entry == nullptr)
	{
		mEntries.emplace_back();
		entry = &mEntries.back();
		strncpy(entry->mName, name, 6);
		uint8_t i=0;
		for (; (i < 6) && (name[i] != '\0'); i++)
		{
			entry->mName[i] =  name[i];
		}
		for (; i < 6; i++)
		{
			entry->mName[i] = '\0';
		}
	}
	
	// Mark as edited
	entry->mEdited = true;
	mEdited = true;
	
	if (data == nullptr)
	{
		entry->mData.resize(0);
		return entry;
	}
	
	// Copy data (pad to even size)
	entry->mData.resize(size + (size % 2), 0xFF);
	memcpy(&entry->mData[0], data, size);
	
	return entry;
}
