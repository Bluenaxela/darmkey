#include <unordered_map>
#include <string>
#include <cstdint>

namespace CharsetYs1Font
{
	static const wchar_t ys1CharMap[193] = L" !\"セキダエ'()ォブザ-ユ/0123456789:;<=>?◎ABCDEFGHIJKLMNOPQRSTUVWXYZ[ぺ]^_ バドデピがぎァィぐェげャュごッーアイざじオカギクずゴサジスゼぜタぞだテトナニぢネづハビフでどマばムメモびぶべラリルレロぼンぱぽ 。『』、・をぉぃぅぇぉゃゅょっ~あいうえおかきくけこさしすせそたちつてとなにぬねのはひふへほまみむめもやゆよらりるれろわん゛゜";

	inline std::wstring decodeString(const std::vector<uint8_t>& s, bool assumeEnFont)
	{
		bool altMode = false;
		std::wstring str;
		for (uint8_t c : s)
		{
			if (((char)c) == '\r') c = '\n';
			if (((char)c) == '@')
			{
				altMode = !altMode;
				continue;
			}
			if (altMode && (c >= 0xA0) && (c <= 0xDF))
			{
				c -= 0x40;
			}
			
			if (((char)c) == '\n')
			{
				str += L"\n";
			}
			else if (assumeEnFont && (((c >= 'a') && (c <= 'z')) || (c == ',') || (c == '.')))
			{
				wchar_t tmp[2];
				tmp[0] = c;
				tmp[1] = L'\00';
				str += std::wstring(tmp);
			}
			else if ((c >= 0x20) && (c <= 0xDF))
			{
				wchar_t tmp[2];
				tmp[0] = ys1CharMap[c - 0x20];
				tmp[1] = L'\00';
				str += std::wstring(tmp);
			}
		}
		return str;
	}

	inline void encodeString(std::vector<uint8_t>& s, const std::wstring& str)
	{
		static std::unordered_map<wchar_t, uint8_t> reverseCharMap;
		static bool bInitCharMap = false;
		if (!bInitCharMap)
		{
			for (int i=0; ys1CharMap[i] != 0; i++)
			{
				if (reverseCharMap.find(ys1CharMap[i]) == reverseCharMap.end())
				{
					reverseCharMap[ys1CharMap[i]] = i + 0x20;
				}
			}
			bInitCharMap = true;
		}
		
		wchar_t lastC = 0;
		for (wchar_t c : str)
		{
			auto searchResult = reverseCharMap.find(c);
			if ((c == L'\r') || (c == L'\n'))
			{
				if (!((c == L'\n') && (lastC == L'\r')))
				{
					s.push_back((uint8_t)'\r');
				}
			}
			else if (searchResult != reverseCharMap.end())
			{
				s.push_back(searchResult->second);
			}
			else if ((c >= 0x20) && (c < 128))
			{
				s.push_back((uint8_t)c);
			}
			lastC = c;
		}
	}

}
