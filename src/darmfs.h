#ifndef _DARMFS_H_
#define _DARMFS_H_

#include <cstdint>
#include <vector>

class D88Image;

class DarmFS
{
public:
	struct Entry
	{
		char mName[7];
		bool mEdited;
		std::vector<uint8_t> mData;
	};

	typedef std::vector<Entry>::size_type size_type;
	typedef std::vector<Entry>::iterator iterator;
	typedef std::vector<Entry>::const_iterator const_iterator;
	
private:
	int mInitTrack;
	int mInitSide;
	int mInitSector;
	std::vector<Entry> mEntries;
	bool mEdited;
	
private:
	Entry* getFileInternal(const char* name);
	const Entry* getFileInternalConst(const char* name) const;
	
public:
	DarmFS();
	~DarmFS();
	
	void load(D88Image& img, uint32_t disk);
	void save(D88Image& img, uint32_t disk);
	
	size_type size() const noexcept { return mEntries.size(); }
	Entry& operator[](size_type idx) { return mEntries[idx]; }
	iterator begin() noexcept { return mEntries.begin(); }
	const_iterator cbegin() const noexcept { return mEntries.cbegin(); }
	iterator end() noexcept { return mEntries.end(); }
	const_iterator cend() const noexcept { return mEntries.cend(); }
	
	const Entry* getFile(const char* name) const { return getFileInternalConst(name); }
	bool deleteFile(const char* name);
	Entry* writeFile(const char* name, const uint8_t* data, size_type size);
	
	bool isEdited() { return mEdited; }
};


#endif // _DARMFS_H_
