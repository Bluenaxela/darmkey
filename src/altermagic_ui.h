#ifndef _ALTERMAGIC_UI_H_
#define _ALTERMAGIC_UI_H_

#include <nana/gui.hpp>
#include <nana/gui/filebox.hpp>
#include <nana/gui/widgets/label.hpp>
#include <nana/gui/widgets/listbox.hpp>
#include <nana/gui/widgets/button.hpp>
#include <nana/gui/widgets/checkbox.hpp>
#include <nana/paint/graphics.hpp>
#include <nana/gui/widgets/textbox.hpp>
#include <nana/gui/widgets/scroll.hpp>
#include <nana/gui/widgets/panel.hpp>
#include <memory>
#include <string>
#include <vector>
#include "darmfs.h"
#include "d88.h"
#include "altermagic.h"

namespace AlterMagic
{
	class AlterMagicUI
	{
	public:
		nana::form mForm;
		
		D88Image mReferenceImg;
		bool mShowing;
		std::string mCurrentFilename;
		uint32_t mCurrentDisk;
		bool mUnsaved;
		
		TextFile mTextFile;
		
		nana::panel<false> mPanel;
		nana::listbox mList;
		
		nana::button mSaveButton;
		nana::label mReferenceLabel;
		nana::button mReferenceBrowse;
		nana::label mMetaLabel;
		nana::filebox mOpenFBox;
		
		nana::checkbox mModEncCheckbox;
		
		nana::button copyTextButton;
		nana::button copyRefButton;
		
	public:
		AlterMagicUI(const nana::form& fm);
		~AlterMagicUI();
		void show(DarmFS& fs, uint32_t disk, const std::string& filename);
		void hide();
		
		void setColumnForData(const std::vector<std::wstring>& strings, uint32_t column);
		
		void tryLoadReference();
		void setTitle();
		
		void saveToDarmKey();
		
		void copyRowsToClipboard(uint32_t col);
	};
}

#endif // _ALTERMAGIC_UI_H_
